#examination
<h4>一、介绍</h4>
<p>项目基于JFinal框架、Mysql数据库、前端框架基于AmazeUI、
视图渲染引擎JSP。
</p>
<h4>二、项目要求</h4>
<p>JDK版本1.8及以上
<br>
Tomcat服务器1.7及以上
<br>
Mysql数据库
</p>

 <h4>三、eclipse修改配置详见</h4>
<p><a href="http://www.w3cschool.cn/jfinal/ia1z1qjd.html">http://www.w3cschool.cn/jfinal/ia1z1qjd.html</a></p>
<h4>四、技术相关文档</h4>
<p>
Jfinal文档及文件<a href="http://pan.baidu.com/s/1nvE2LRF">http://pan.baidu.com/s/1nvE2LRF</a>
<br>
JFinal社区:<a href="http://www.jfinal.com/">http://www.jfinal.com/</a>
<br>
AmazeUI:<a href = "http://amazeui.org/">http://amazeui.org/</a>
</p>
<h4>四、改动清单</h4>
<p>(一)、科目管理：</p><br>
<p>增加全局Handler，过滤XSS</p><br>
<p>数据库表:courses(id,name);主键为id，name为科目名，设置科目唯一约束且不能为空，免去判断是否重名。</p><br>
<p>CourseManage.jsp:主要改动为对界面的美化，将原AJAX替换为jquery方式。</p><br>
<p>可继续优化的地方：增加分页。添加日志；将所有的验证方法统一写在一个文件夹中便于管理；
</p>

