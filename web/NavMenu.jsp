<%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/5/26 0026
  Time: 16:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header class="am-topbar am-topbar-inverse admin-header">
    <div>
        <div class="am-topbar-brand">
            <a href="/index" class="tpl-logo">
                <img src="/assets/img/logo.png" alt="">
            </a>
        </div>
        <div class="am-collapse am-topbar-collapse" id="topbar-collapse">

            <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list tpl-header-list">
                <li class="am-dropdown" data-am-dropdown data-am-dropdown-toggle>
                </li>
                <li class="am-dropdown" data-am-dropdown data-am-dropdown-toggle>
                </li>
                <li class="am-dropdown" data-am-dropdown data-am-dropdown-toggle>
                </li>
                <li class="am-hide-sm-only"><a href="javascript:;" id="admin-fullscreen"
                                               class="tpl-header-list-link"><span
                        class="am-icon-arrows-alt"></span> <span class="admin-fullText">开启全屏</span></a></li>

                <li class="am-dropdown" data-am-dropdown data-am-dropdown-toggle>
                    <a class="am-dropdown-toggle tpl-header-list-link" href="javascript:;">
                        <span class="tpl-header-list-user-nick">系统管理员</span><span class="tpl-header-list-user-ico"> <img
                            src="/assets/img/user01.png"></span>
                    </a>
                    <ul class="am-dropdown-content">
                        <li><a href="#"><span class="am-icon-power-off"></span> 退出</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</header>
<div class="tpl-page-container tpl-page-header-fixed">
    <div class="tpl-left-nav tpl-left-nav-hover">
        <div class="tpl-left-nav-title">
            菜单栏
        </div>
        <div class="tpl-left-nav-list">
            <ul class="tpl-left-nav-menu">

                <li class="tpl-left-nav-item">
                    <a href="/courses/showCourses" class="nav-link tpl-left-nav-link-list">
                        <i class="am-icon-bar-chart"></i>
                        <span>科目管理</span>
                    </a>
                </li>
                <li class="tpl-left-nav-item">
                    <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                        <i class="am-icon-table"></i>
                        <span>题库管理</span>
                        <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right"></i>
                    </a>
                    <ul class="tpl-left-nav-sub-menu">
                        <li>
                            <a href="/tiku/showCourses">
                                <i class="am-icon-angle-right"></i>
                                <span>题库生成</span>
                                <i class=" tpl-left-nav-content-ico am-fr am-margin-right"></i>
                            </a>
                            <a href="/tikuManage/findTiku">
                                <i class="am-icon-angle-right"></i>
                                <span>题库管理</span></a>
                            <a href="/tikuManage/findKemu">
                                <i class="am-icon-angle-right"></i>
                                <span>题库公开</span>

                            </a>
                        </li>
                    </ul>
                </li>
                <li class="tpl-left-nav-item">
                    <a href="/mujuan/showMujuan" class="nav-link tpl-left-nav-link-list">
                        <i class="am-icon-wpforms"></i>
                        <span>母卷管理</span>
                    </a>
                </li>

                <li class="tpl-left-nav-item">
                    <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                        <i class="am-icon-wpforms"></i>
                        <span>考试设置</span>
                        <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right tpl-left-nav-more-ico-rotate"></i>
                    </a>
                    <ul class="tpl-left-nav-sub-menu" >
                        <li>
                            <a href="/student/findAllStu">
                                <i class="am-icon-angle-right"></i>
                                <span>考生管理</span>
                                <i class="tpl-left-nav-content-ico am-fr am-margin-right"></i>
                            </a>

                            <a href="/view/exam/PaperManage.jsp">
                                <i class="am-icon-angle-right"></i>
                                <span>试卷分配</span>
                            </a>
                            <a href="/exam/show">
                                <i class="am-icon-angle-right"></i>
                                <span>设置考试</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="tpl-left-nav-item">
                    <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                        <i class="am-icon-wpforms"></i>
                        <span>考试阅卷</span>
                        <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right tpl-left-nav-more-ico-rotate"></i>
                    </a>
                    <ul class="tpl-left-nav-sub-menu" >
                        <li>
                            <a href="form-amazeui.html">
                                <i class="am-icon-angle-right"></i>
                                <span>自动评分</span>
                                <i class="tpl-left-nav-content-ico am-fr am-margin-right"></i>
                            </a>

                            <a href="form-line.html">
                                <i class="am-icon-angle-right"></i>
                                <span>分数查询</span>
                            </a>
                            <a href="form-line.html">
                                <i class="am-icon-angle-right"></i>
                                <span>成绩导出</span>
                            </a>
                            <a href="form-line.html">
                                <i class="am-icon-angle-right"></i>
                                <span>成绩发布</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="tpl-left-nav-item">
                    <a href="login.html" class="nav-link tpl-left-nav-link-list">
                        <i class="am-icon-key"></i>
                        <span>系统帮助</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="tpl-content-wrapper">
    </div>
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/amazeui.min.js"></script>
<script src="/assets/js/iscroll.js"></script>
<script src="/assets/js/app.js"></script>
<script src="/assets/js/jquery.form.js"></script>