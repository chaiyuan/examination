<%@ page import="com.model.Course" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/5/23 0023
  Time: 20:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/jquery.min.js"></script>
</head>

<body data-type="generalComponents">

<%@include file="/NavMenu.jsp" %>
<div style="float: left;margin-left: 2%;width: 76%">
    <div class="tpl-portlet-components">
        <div class="portlet-title">
            <div class="caption font-green bold">
                <span class="am-icon-code"></span> 科目管理
            </div>
        </div>
        <div class="tpl-block">
            <div class="am-g">
                <div class="am-u-sm-12">

                    <table class="am-table  am-table-hover am-text-center table-main ">
                        <button type="button" style="float: right" class="am-btn am-btn-default am-btn-success am-btn-xs" data-am-modal="{target: '#add-confirm'}">
                            <span class="am-icon-plus"></span>新增</button>
                        <tr class="am-primary">
                            <th class="table-id" style="text-align: center" width="10%">ID</th>
                            <th class="table-title" style="text-align: center" width="60%">名称</th>
                            <th class="table-set" style="text-align: center">操作</th>
                        </tr>
                        <tbody>
                        <%
                            List<Course> CourseList = (List<Course>) request.getAttribute("CourseList");
                            Iterator<Course> it = CourseList.iterator();
                            int j = 0;
                            while (it.hasNext()) {
                                Course course = it.next();
                                int id = course.getInt("id");
                                String name = course.getStr("name");
                                j++;
                        %>
                        <tr id="tr<%=j%>">
                            <td width="10%"><%=j%>
                            </td>
                            <td id="op<%=j%>" width="60%"><%=name%>
                            </td>
                            <td align="center">
                                <div class="am-btn-toolbar" style="float: none">
                                    <div class="am-btn-group " style="float: none">
                                        <button class="am-btn am-btn-default am-btn-xs am-text-secondary"
                                                onclick="onUpdate(<%=id%>,'<%=name%>')"><span
                                                class="am-icon-pencil-square-o"></span> 编辑
                                        </button>
                                        <button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only"
                                                onclick="onDel(<%=id%>)"><span
                                                class="am-icon-trash-o"></span> 删除
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        </tbody>

                    </table>
                    <hr>
                    <%--新增模态窗口--%>
                    <div class="am-modal am-modal-confirm" tabindex="-1" id="add-confirm">
                        <div class="am-modal-dialog">
                            <div class="am-modal-hd">新增科目</div>
                            <div class="am-modal-bd">
                                <label for="subject">科目名称:</label>
                                <input type="text" id="subject_name" placeholder="格式为：年级-科目" class="am-modal-prompt-input">
                            </div>
                            <div class="am-modal-footer">
                                <span class="am-modal-btn" data-am-modal-cancel>取消</span>
                                <span class="am-modal-btn" id="commit-btn">确定</span>
                            </div>
                        </div>
                    </div>
                    <!--  修改模态窗口-->
                    <div class="am-modal am-modal-confirm" tabindex="-1" id="my-confirm">
                        <div class="am-modal-dialog">
                            <div class="am-modal-hd">编辑科目</div>
                            <div class="am-modal-bd">
                                <label for="subject">科目名称:</label>
                                <input type="text" id="subject" class="am-modal-prompt-input">
                                <input type="hidden" id="subject_id" class="am-modal-prompt-input">
                            </div>
                            <div class="am-modal-footer">
                                <span class="am-modal-btn" data-am-modal-cancel>取消</span>
                                <span class="am-modal-btn" id="confirm-btn">确定</span>
                            </div>
                        </div>
                    </div>
                    <%--删除模态窗口--%>
                    <div class="am-modal am-modal-confirm" tabindex="-1" id="del-confirm">
                        <div class="am-modal-dialog">
                            <div class="am-modal-hd">警告:</div>
                            <div class="am-modal-bd">
                                你，确定要删除此科目吗？
                                <input type="hidden" id="subject_del_id" class="am-modal-prompt-input">
                            </div>
                            <div class="am-modal-footer">
                                <span class="am-modal-btn" data-am-modal-cancel>取消</span>
                                <span class="am-modal-btn" id="del-btn">确定</span>
                            </div>
                        </div>
                    </div>
                    <%--alert 模态窗口--%>
                    <div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
                        <div class="am-modal-dialog">
                            <div class="am-modal-hd">信息提示:</div>
                            <div class="am-modal-bd" id="message">

                            </div>
                            <div class="am-modal-footer">
                                <span class="am-modal-btn" onclick="ref()">确定</span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="tpl-alert"></div>
    </div>
</div>
<script>
    $(function () {
        $('#confirm-btn').on('click', function () {
            var newOperatorName = document.getElementById("subject").value;
            if ("" == newOperatorName) {
                document.getElementById("message").innerHTML="科目不能为空";
                $('#my-alert').modal('open');
                return false;
            }
            if (newOperatorName.indexOf("-") == -1) {
                document.getElementById("message").innerHTML="科目名称格式不正确，请重新输入！";
                $('#my-alert').modal('open');
                return false;
            }
            var id = $('#subject_id').val();
            $.ajax({
                method: "post",
                url: "/courses/editCourse",
                data: {
                    "operatorId": id,
                    "operatorName": newOperatorName
                },
                dataType: "json",
                success: function (res) {
                    if (res.result == "success") {
                        document.getElementById("message").innerHTML = "修改成功";

                    } else {
                        document.getElementById("message").innerHTML = "修改失败，请检查是否重名";
                    }
                    $('#my-alert').modal('open');
                }
            });

        });
        $('#commit-btn').on('click', function () {
            var newOperatorName = document.getElementById("subject_name").value;
            if ("" == newOperatorName) {
                document.getElementById("message").innerHTML="科目不能为空";
                $('#my-alert').modal('open');
                return false;
            }
            if (newOperatorName.indexOf("-") == -1) {
                document.getElementById("message").innerHTML="科目格式不正确";
                $('#my-alert').modal('open');
                return false;
            }

            $.ajax({
                method: "post",
                url: "/courses/addCourse",
                data: {
                    "kemu": newOperatorName
                },
                dataType: "json",
                success: function (res) {

                        document.getElementById("message").innerHTML = res.result;
                    $('#my-alert').modal('open');
                }
            });

        });
        $('#del-btn').on('click', function () {
            var id = document.getElementById("subject_del_id").value;
            $.ajax({
                method: "get",
                url: "/courses/deleteCourse/"+id,
                success: function (res) {
                    if (res.result == "success") {
                        document.getElementById("message").innerHTML = "删除成功";

                    } else {
                        document.getElementById("message").innerHTML = "删除失败";
                    }
                    $('#my-alert').modal('open');
                }
            });

        });
    });
    function onUpdate(id, name) {
        document.getElementById('subject').setAttribute("placeholder", name);
        document.getElementById('subject_id').setAttribute("value", id);

        $('#my-confirm').modal('open');
    }
    function onDel(id) {
        document.getElementById('subject_del_id').setAttribute("value", id);
        $('#del-confirm').modal('open');
    }
    function ref() {
        window.location.href = "/courses/showCourses";
    }

</script>
</body>

</html>
