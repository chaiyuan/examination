<%@ page import="com.model.PaperConfig" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/6/22 0022
  Time: 9:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/jquery.min.js"></script>
    <script>

    </script>
</head>
<body>
<%@ include file="/NavMenu.jsp" %>
<div style="float: left;margin-left: 2%;width: 76%">
    <div class="tpl-portlet-components">
        <div class="portlet-title">
            <div class="caption font-green bold">
                <span class="am-icon-code"></span> 母卷管理
            </div>
        </div>
        <div class="tpl-block">
            <div style="float: right">
                <button type="button" class="am-btn am-btn-primary" id="zujuans_btn">一键组卷</button>
                <button type="button" class="am-btn am-btn-secondary" id="zujuan_btn">自动生成母卷</button>
                <button type="button" class="am-btn am-btn-success" id="modify_btn">修改母卷</button>
            </div>
            <br/>
            <br/>
            <div>
                <table align="center" class="am-table am-table-bordered am-table-hover" border="solid">
                    <tr class="am-primary">
                        <td class="head" colspan="3" align="center">已生成的母卷</td>
                    </tr>
                    <tr align="center">
                        <td>科目</td>
                        <td>场次</td>
                        <td>删除</td>
                    </tr>
                    <%
                        List<PaperConfig> paperList = (List<PaperConfig>) request.getAttribute("mujuanAllList");
                        for (PaperConfig paper : paperList) {
                    %>
                    <tr align="center">
                        <td><%=paper.getCourse().get("name")%>
                        </td>
                        <td><%=paper.get("exam_no")%>
                        </td>
                        <td>
                            <button type="button" id="delete_btn" class="am-btn am-btn-danger am-round"
                                    onclick="deleteMujuan(<%=paper.get("id")%>)">删除
                            </button>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </table>
            </div>
        </div>
    </div>
</div>
<!--生成母卷模态窗口-->
<div class="am-modal am-modal-prompt" tabindex="-1" id="zujuan_window">
    <div class="am-modal-dialog">
        <div class="am-modal-hd">生成母卷</div>
        <form action="zujuan" method="post" id="zujuan_form">
            <select data-am-selected id="zujuan_select" name="courseId">
                <option value="--" disabled>----</option>
                <c:forEach items="${kemuList}" var="kemu">
                    <option value="${kemu.id}">${kemu.name}</option>
                </c:forEach>
            </select>
        </form>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" id="zujuan_confirm">提交</span>
        </div>
    </div>
</div>
<!--修改母卷模态窗口-->
<div class="am-modal am-modal-prompt" tabindex="-1" id="update_window">
    <div class="am-modal-dialog">
        <div class="am-modal-hd">修改母卷</div>
        <form action="showModify" method="post" id="modify_form">
            <select data-am-selected id="update_select" name="courseId">
                <option value="--" disabled>科目</option>
                <c:forEach items="${zujuanKemuList}" var="kemu">
                    <option value="${kemu.id}">${kemu.name}</option>
                </c:forEach>
            </select>

            <select data-am-selected id="update_select_type" name="timuType">
                <option value="1" selected>选择题</option>
                <option value="2">判断题</option>
                <option value="3">简单填空</option>
                <option value="4">复杂填空</option>
                <option value="5">简答题</option>
                <option value="6">资料分析</option>
                <option value="7">多项选择</option>
                <option value="8">公文写作</option>
                <option value="9">阅读选择</option>
            </select>
            <br>
            场次：<input type="text" id="changci_update" name="changci" value="1">
        </form>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" id="update_confirm">提交</span>
        </div>
    </div>
</div>
<!--一键组卷模态窗口-->
<div class="am-modal am-modal-prompt" tabindex="-1" id="zujuans_window">
    <div class="am-modal-dialog">
        <div class="am-modal-hd">修改母卷</div>
        <form action="doSimpleZujuan" method="post" id="OneKey_form">
            <select data-am-selected id="OneKey_select" name="courseId" onchange="getPaper()">
                <option value="--">年级</option>
                <c:forEach items="${grades}" var="grade">
                    <option value="${grade}">${grade}</option>
                </c:forEach>
            </select>
            <br>
            <br>
            <div  style="width: 100%;">
                <table id="table" class="am-table" ></table>
                <input type="hidden" id="yuwen" name="yuwenId" value=""/>
                <input type="hidden" id="shuxue" name="shuxueId" value=""/>
                <input type="hidden" id="zhengzhi" name="zhengzhiId" value=""/>
            </div>
            <br>
        </form>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" id="OneKey_confirm">提交</span>
        </div>
    </div>
</div>

<!--确认删除模态窗口-->
<div class="am-modal am-modal-confirm" tabindex="-1" id="confirm_del">
    <div class="am-modal-dialog">
        <input id="delete_id" value="" type="hidden">
        <div class="am-modal-bd">
            你，确定要删除这条记录吗？
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" id="confirmdel_btn">确定</span>
        </div>
    </div>
</div>
<!--alert模态窗口-->
<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
    <div class="am-modal-dialog">
        <div class="am-modal-bd" id="message">
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" id="alert_btn">确定</span>
        </div>
    </div>
</div>
<script>
    function deleteMujuan(paperId) {
        document.getElementById("delete_id").value = paperId;
        $('#confirm_del').modal('open');
    }
    function getPaper() {
        $('.rdo').remove();
        var vs = $('#OneKey_select  option:selected').val();
        $.ajax({
            url: 'simpleMake',
            data: 'grade=' + encodeURI(encodeURI(vs)),
            cache: false,
            method: 'post',
            success: function (json) {
                if (json == '本年级某科目已存在母卷，请先删除母卷') {
                    document.getElementById("message").innerText = json;
                    $('#my-alert').modal("open");
                    return;
                }
                var a = json;
                var yuwen = a.yuwenList;
                var shuxue = a.shuxueList;
                var zhengzhi = a.zhengzhiList;
                var yuwenText = "";
                var shuxueText = "";
                var zhengzhiText = "";
                $('#yuwen').val(a.yuwen.id);
                $('#shuxue').val(a.shuxue.id);
                $('#zhengzhi').val(a.zhengzhi.id);
                for (var yw = 0; yw < yuwen.length; yw++) {
                    yuwenText += "套题" + yuwen[yw] + ":<input type='radio' name='yuwen' value='" + yuwen[yw] + "'/>&nbsp&nbsp&nbsp&nbsp";
                }
                for (var sx = 0; sx < shuxue.length; sx++) {
                    shuxueText += "套题" + shuxue[sx] + ":<input type='radio' name='shuxue' value='" + shuxue[sx] + "'/>&nbsp&nbsp&nbsp&nbsp";
                }
                for (var zz = 0; zz < zhengzhi.length; zz++) {
                    zhengzhiText += "套题" + zhengzhi[zz] + ":<input type='radio' name='zhengzhi' value='" + zhengzhi[zz] + "'/>&nbsp&nbsp&nbsp&nbsp";
                }
                $('#table').append("<tr class='rdo'>" +
                    "<td width='30%' align='right'>语文:</td><td width='70%'>" + yuwenText + "</td>"
                    + "</tr>");
                $('#table').append("<tr class='rdo'>" +
                    "<td align='right'>数学:</td><td>" + shuxueText + "</td>"
                    + "</tr>");
                $('#table').append("<tr class='rdo'>" +
                    "<td align='right'>政治:</td><td>" + zhengzhiText + "</td>"
                    + "</tr>");
                json = null;
            },
            error: function () {
                alert('error');
            }
        });
    }
    $(function () {
        //自动生成母卷
        $('#zujuan_btn').on('click', function () {
            $('#zujuan_window').modal('open');
        });
        $('#zujuan_confirm').on('click', function () {
            var courseId = $('#zujuan_select option:selected').val();
            if (courseId == "--") {
                return false;
            }
            document.getElementById("zujuan_form").submit();
        });
        $('#confirmdel_btn').on('click', function () {
            var mujuanId = document.getElementById("delete_id").value;
            $.ajax({
                method: "post",
                url: "deleteMujuan",
                data: {
                    mujuanId: mujuanId
                },
                success: function (res) {
                    document.getElementById("message").innerText = res;
                    $('#my-alert').modal('open');
                },
                error: function (res) {
                    alert(res);
                }
            });
        });
        $('#alert_btn').on('click', function () {
            window.location.href = "showMujuan";
        });

        //修改母卷
        $('#modify_btn').on('click', function () {
            $('#update_window').modal('open');
        });
        $('#update_confirm').on('click', function () {
            var regex = /^\+?[0-9][0-9]*$/;
            var changci = document.getElementById("changci_update").value;
            if (regex.test(changci)) {
                document.getElementById("modify_form").submit();
            } else {
                document.getElementById("message").innerHTML = "场次须为正整数";
                $('#my-alert').modal('open');
            }
        });
        //一键组卷
        $('#zujuans_btn').on('click', function () {
            $('#zujuans_window').modal('open');
        });
        $('#OneKey_confirm').on('click',function(){
           //document.getElementById("OneKey_form").submit();
            $.ajax({
                method:"post",
                data:$('#OneKey_form').serialize(),
                url:"doSimpleZujuan",
                success:function (res) {
                    document.getElementById("message").innerText=res;
                    $('#my-alert').modal('open');
                }
            })
        });
    });

</script>
</body>
</html>
