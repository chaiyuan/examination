<%@ page import="java.util.List" %>
<%@ page import="com.jfinal.plugin.activerecord.Record" %><%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/6/27 0027
  Time: 14:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/jquery.min.js"></script>
</head>
<body>
<%@ include file="/NavMenu.jsp" %>
<div style="float: left;margin-left: 2%;width: 76%">
    <div class="tpl-portlet-components">
        <div class="portlet-title">
            <div class="caption font-green bold" style="width: 100%;">
                <span class="am-icon-code" style="float: left"></span> 请选择替代试题<span style="float: right"><button type="button" class="am-btn am-btn-secondary" id="back_btn">返回</button></span>
            </div>
        </div>
        <div class="tpl-block">
            <div class="am-panel-group" id="accordion">
                <c:forEach var="question" varStatus="quesStatus" items="${questions}">
                    <c:choose>
                        <c:when test="${type eq 1 or type eq 9}">
                            <div class="am-panel am-panel-default am-panel-success">
                                <div class="am-panel-hd"
                                     data-am-collapse="{parent: '#accordion', target: '#timu${quesStatus.index}'}">
                                    <p>题干:${question.body}</p>
                                </div>
                                <div id="timu${quesStatus.index}" class="am-panel-collapse am-collapse">
                                    <table class="am-table">
                                            <tr>
                                                <input type="hidden" name="oldId" value="${oldId}"/>
                                                <input type="hidden" name="exam_no" value="${changci}"/>
                                                <input type="hidden" name="type" value="${type}"/>
                                                <input type="hidden" name="course_id" value="${course.id}"/>
                                                <th width="50px" class="bg">题号：</th>
                                                <td><input type="hidden" name="id"
                                                           value="${question.id}"/>${quesStatus.index+1}</td>
                                                <th>分值:</th>
                                                <td>${question.score}</td>
                                                <th>章节：</th>
                                                <td><input type="hidden" name="chapter"
                                                           value="${quesiont.chapter}"/>${question.chapter}</td>
                                                <th>难度：</th>
                                                <td><input type="hidden" name="rate"
                                                           value="${question.rate}"/>${question.rate}</td>
                                                <th>相似度：</th>
                                                <td><input type="hidden" name="sameLevel"
                                                           value="${question.samelevel}"/>${question.samelevel}</td>
                                                <td>
                                                    <input type="button" class="am-btn am-btn-success" value="替换" onclick="replace(${question.id})"/>
                                                </td>
                                            </tr>

                                        <tr>
                                            <th>A</th>
                                            <td colspan="7">${question.a}</td>
                                            <th>B
                                            </td>
                                            <td colspan="4">${question.b}</td>
                                        </tr>
                                        <tr>
                                            <th>C</th>
                                            <td colspan="7">${question.c}</td>
                                            <th>D
                                            </td>
                                            <td colspan="4">${question.d}</td>
                                        </tr>
                                        <tr>
                                            <th>图片：</th>
                                            <td colspan="12">${question.imagename}</td>
                                        </tr>
                                        <tr>
                                            <th class="bg">答案：</th>
                                            <td colspan="12">${question.answer}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                                <input type="hidden" name="oldId" value="${oldId}"/>
                                <input type="hidden" name="exam_no" value="${changci}"/>
                                <input type="hidden" name="type" value="${type}"/>
                                <input type="hidden" name="course_id" value="${course.id}"/>
                                <tr>
                                    <th width="50px" class="bg">题号：</th>
                                    <td><input type="hidden" name="id" value="${question.id}"/>${quesStatus.index+1}
                                    </td>
                                    <td>分值：</td>
                                    <td>${question.score}
                                </tr>
                            <tr>
                                <td colspan="4">
                                    <table width="100%">
                                        <tr>
                                            <td width="80px"><font class="bg">章节：</font></td>
                                            <td><input type="hidden" name="chapter"
                                                       value="${question.chapter}"/>${question.chapter}</td>
                                            <td width="80px"><font class="bg">难度：</font></td>
                                            <td><input type="hidden" name="rate"
                                                       value="${question.rate}"/>${question.rate}
                                            </td>
                                            <td width="80px"><font class="bg">相似度：</font></td>
                                            <td><input type="hidden" name="sameLevel"
                                                       value="${question.samelevel}"/>${question.samelevel}
                                            </td>
                                            <td>
                                                <input type="button" class="am-btn am-btn-success" value="替换" onclick="replace(${question.id})"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="bg">图片：</td>
                                <td colspan="3">${question.imagename}
                                </td>
                            </tr>
                            <tr>
                                <td class="bg">答案：</td>
                                <td colspan="3">${question.answer}
                                </td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<!--alert模态窗口-->
<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
    <div class="am-modal-dialog">
        <div class="am-modal-bd" id="message">
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" id="alert_btn">确定</span>
        </div>
    </div>
</div>
<script>

    function replace(id){
        $.ajax({
            url:"replaceTimu",
            method:"post",
            data:{
                oldId:"${oldId}",
                id:id
            },
            success:function (res) {
                document.getElementById("message").innerText=res;
                $('#my-alert').modal('open');
            }
        })
    }

    $(function () {
        $('#alert_btn').on('click',function () {
            window.location.href="showModify?courseId=${course.id}&changci=${changci}&timuType=${type}";
        });
        $('#back_btn').on('click',function () {
            window.location.href="showModify?courseId=${course.id}&changci=${changci}&timuType=${type}";
        });
    });
</script>
</body>
</html>
