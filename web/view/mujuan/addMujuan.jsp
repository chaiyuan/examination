<%@ page import="com.utils.ConstData" %><%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/6/23 0023
  Time: 17:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/echarts.min.js"></script>
    <script language="JavaScript">
        function isInt(row){
            var num=parseInt(document.getElementById("timuNum_"+row).value);
            var regex=/^\+?[0-9][0-9]*$/;
            if(regex.test(num)){//判断row是否为非负整数
                var maxNum=parseInt(document.getElementById("num_"+row).innerText);
                if(num>maxNum){
                    document.getElementById("message").innerHTML="输入题目数量不能大于题目数量";
                    $('#my-alert').modal('open');

                }else{
                    var totalNum=0;
                    for(var i=0;i<${fn:length(zujuanDetail)};i++){
                        totalNum+=parseInt(document.getElementById("timuNum_"+i).value);
                    }
                    document.getElementById('sum').innerHTML=totalNum;
                }
            }else{
                document.getElementById("message").innerHTML="请输入整数数字";
                $('#my-alert').modal('open');
            }
        }
        function checkSubmit() {
            //试卷题目、场次为正整数
            var regex=/^\+?[1-9][0-9]*$/;
            var sum=document.getElementById("sum").innerText;
            var changci=document.getElementById("changci").value;
            if(!regex.test(sum)){
                document.getElementById("message").innerText="试卷题目需为正整数";
                $('#my-alert').modal('open');
                return false;
            }else if(regex.test(changci)){
                return true;
            }else{
                document.getElementById("message").innerText="考试场次需为正整数";
                $('#my-alert').modal('open');
                return false;
            }

        }

    </script>
</head>
<body>
<%@include file="/NavMenu.jsp" %>

<div class="tpl-portlet-components" style="float: left;margin-left: 2%;width: 76%">
    <div class="portlet-title">
        <div class="caption font-green bold">
            <span class="am-icon-code"></span> 题库修改
        </div>
    </div>
    <div class="tpl-block">
        <form action="doZujuan" method="post">
        <table align="center" class="am-table am-table-bordered am-table-hover" border="solid">
            <tr>
                <th class="head" colspan="6">科目：${course.name}</th>
                <input type="hidden" name="courseId" value="${course.id}">
            </tr>
            <tr>
                <th>题目类型</th>
                <th>难度系数</th>
                <th>所属章节</th>
                <th>知识点</th>
                <th>题目数量</th>
                <th>试卷题目数量</th>
            </tr>
            <c:forEach items="${zujuanDetail}" var="zujuan" varStatus="zujuanStatus">
                <tr>
                    <td>${zujuan.timuType}</td>
                    <td>${zujuan.rate}</td>
                    <td>${zujuan.chapter}</td>
                    <td>${zujuan.point}</td>
                    <td id="num_${zujuanStatus.index}">${fn:length(zujuan.questionID)}</td>
                    <td><input type="text" name="giveNumber" value="0" onblur="isInt('${zujuanStatus.index}')"
                               id="timuNum_${zujuanStatus.index}"></input>
                    </td>
                </tr>

            </c:forEach>

            <input type="hidden" id="size" value=""/>
            <tr>
                <td colspan="2">题目数总计：</td>
                <td id="sum" colspan="2">0</td>
                <td colspan="1">要分配场次数目：</td>
                <td colspan="1"><input name="changci" id="changci" type="text" value="0"></input>
                </td>

            </tr>
            <input type="hidden" name="courseID" value=""/>
            <tr align="center">
                <td colspan="6">
                    <input type="submit" value="提交" onclick="return checkSubmit()" class="am-btn am-btn-success"></input>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="reset" value="清除"  class="am-btn am-btn-danger "></input>
                </td>
            </tr>
        </table>
        </form>
    </div>
</div>
<!--alert模态窗口-->
<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
    <div class="am-modal-dialog">
        <div class="am-modal-bd" id="message">
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" id="alert_btn">确定</span>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#alert_btn').on('click', function () {
            window.location.href = "showMujuan";
        });
    });
</script>
<%
    String message=(String) request.getAttribute("result");
    if(message!=null&&!"".equals(message)){
%>
    <script language="JavaScript">
        document.getElementById("message").innerHTML="<%=message%>";
        $('#my-alert').modal('open');
    </script>
<%
    }
%>
</body>
</html>
