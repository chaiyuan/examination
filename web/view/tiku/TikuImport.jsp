<%@ page import="com.model.Course" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/5/26 0026
  Time: 15:59
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/echarts.min.js"></script>
</head>

<body data-type="generalComponents">
<%@include file="/NavMenu.jsp"%>
<%
    String result=(String) request.getAttribute("result");
    if(result!=null&&result!=""){
%>
<script language="JavaScript">
    alert("<%=result%>");
</script>
<%
    }
%>
<div class="tpl-portlet-components" style="float: left;margin-left: 2%;width: 76%">
    <div class="portlet-title">
        <div class="caption font-green bold">
            <span class="am-icon-code"></span> 题库生成
        </div>
    </div>
    <div class="tpl-block">
        <div style="float: right;width: 70%;margin-right: 10%;">
            <form action="/tiku/downloadTikuMuban"  id="downloadForm" method="post" >
                <table class="am-table am-table-bordered am-table-radius am-table-striped" style="text-align: center;width:80%;" >
                    <tr>
                        <td  colspan="2">下载题库模板</td>
                    </tr>
                    <tr>
                        <td>
                            <button type="button" class="am-btn am-btn-default am-btn-xg am-btn-primary" onclick="{document.getElementById('downloadForm').submit()}">下载</button>
                        </td>
                    </tr>
                </table>
            </form>
            <div style="">
                <form action="/tiku/importTiku"   method="post" enctype="multipart/form-data">
                    <table class="am-table am-table-bordered am-table-radius am-table-striped" style="text-align: center;width:80%;">
                        <tr>
                            <td  colspan="2">导入题库</td>
                        </tr>
                        <tr>
                            <td >
                                科目：<select name="courseId" id="courseId" data-am-selected="{ btnSize: 'sm', btnStyle: 'primary'}">
                                <option value="--" disabled>选择科目</option>
                                <%
                                    List<Course> list=(List<Course>)request.getAttribute("CourseList");
                                    if(list!=null&&!list.isEmpty()){
                                        for(Course c:list){
                                %>
                                <option value="<%=c.get("id")%>"><%=c.get("name")%></option>
                                <%
                                        }
                                    }
                                %>

                            </select>
                            </td>
                            <td >
                                <input type='file' name='file' id='myfile'/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2' align='center'><input type='submit' value='导入' onclick="" class="am-btn am-btn-default am-btn-xg am-btn-primary"/>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='reset' value='重置' class="am-btn am-btn-default am-btn-xg am-btn-primary"/><br></td>
                        </tr>
                    </table>
                </form>
            </div>
            <div style="">
                <form action="/tiku/importPic"   method="post" enctype="multipart/form-data">
                    <table class="am-table am-table-bordered am-table-radius am-table-striped" style="text-align: center;width:80%;">
                        <tr>
                            <td  colspan="2">上传试题相关图片</td>
                        </tr>
                        <tr>
                            <td >
                                <input type='file' name='picFile' id='mypicFile' style="margin-left: 40%;"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2' align='center'><input type='submit' value='上传' class="am-btn am-btn-default am-btn-xg am-btn-primary" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='reset' value='重置' class="am-btn am-btn-default am-btn-xg am-btn-primary"/><br></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>

<%
    String Result=(String) request.getAttribute("Result");
%>
<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
    <div class="am-modal-dialog">
        <div class="am-modal-bd">
            <%=Result%>
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn">确定</span>
        </div>
    </div>
</div>
<%
    if(Result!=null&&Result!=""){
        %>
        <script>
            $('#my-alert').modal('open');
        </script>
<%
    }
%>

</body>

</html>

