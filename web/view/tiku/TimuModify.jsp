<%@ page import="java.io.Console" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/6/9 0009
  Time: 16:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/echarts.min.js"></script>
    <style type="text/css">
        input {
            border-style: solid;


        }
        body {
            color: black;
        }
    </style>
    <script language="JavaScript">
        //判断字符串是否在数组中
        function in_array(arr, value) {
            var i;
            for (i = 0; i < arr.length; i++) {
                if (value === arr[i]) {
                    return i;
                }
            }
            return -1;
        } // 返回-1表示没找到，返回其他值表示找到的索引
         function check() {
            var kind = document.changeCDForm.kind.value;
            var answer = document.changeCDForm.answer.value;
            var score = document.changeCDForm.score.value;
            var  taoti= document.changeCDForm.taoti.value;
            if(kind==1||kind==2||kind==3||kind==4||kind==7){//判断题目是input还是textarea
                var body=document.changeCDForm.body.value;
            }else{
                var body=document.changeCDForm.Areabody.value;
            }
            if(body=null||body.length<=0){
                document.getElementById("Result").innerHTML="题目不能为空";
                $('#my-alert').modal('open');
                return false;
            }
            //分数、答案、套题不能为空
            if ( score == null || score.length <= 0 ||
                answer == null || answer.length <= 0 ||
                taoti == null || taoti.length <= 0) {
                document.getElementById("Result").innerHTML="信息不完整，请补齐";
                $('#my-alert').modal('open');
                return false;
            } else {
                if (isNaN(score)) {
                    document.getElementById("Result").innerHTML="您输入的分数不是数字，请重新输入！";
                    $('#my-alert').modal('open');
                    return false;
                }
                switch (kind) {
                    case '1':
                        var answerArr = ["A", "B", "C", "D"];
                        var test = in_array(answerArr, answer);
                        if (test == -1) {
                            document.getElementById("Result").innerHTML="您的答案不在选项中，请重新输入！";
                            $('#my-alert').modal('open');
                            return false;
                        }
                        return true;
                        break;
                    case '4':
                        var answerArr = ["0", "1"];
                        var test = in_array(answerArr, answer);
                        if (test == -1) {
                            document.getElementById("Result").innerHTML="您的答案不在选项中，请重新输入！";
                            $('#my-alert').modal('open');
                            return false;
                        }
                        return true;
                        break;
                    case '7':
                        var pattern = /^(?!.*(.).*\1)[A-D]{1,4}$/;
                        if (pattern.test(answer) == false) {
                            document.getElementById("Result").innerHTML="您的答案不在选项中，请重新输入！";
                            $('#my-alert').modal('open');
                            return false;
                        }
                        return true;
                        break;
                    default:
                        return true;
                }
            }

        }

        function goback() {
            window.location.href = "/tikuManage/findTiku?type=<%=request.getParameter("kind")%>&course=<%=request.getParameter("courseId")%>&chapter=<%=request.getParameter("chapter")%>&page=${param.page}";
        }
    </script>

</head>
<body>

<%
    int kind = Integer.parseInt((String)request.getAttribute("kind"));
    int courseId = Integer.parseInt((String)request.getAttribute("courseId"));
    if((String)request.getAttribute("chapter")!=null&&!"".equals((String)request.getAttribute("chapter"))){
        int chapter = Integer.parseInt((String)request.getAttribute("chapter"));
    }
    String courseName = java.net.URLDecoder.decode((String)request.getAttribute("courseName"),"UTF-8");
    String q = "";
    switch (kind) {
        case 1:
            q = "选择题";
            break;
        case 2:
            q = "简单填空";
            break;
        case 3:
            q = "复杂填空";
            break;
        case 4:
            q = "判断题";
            break;
        case 5:
            q = "简答题";
            break;
        case 6:
            q = "资料分析";
            break;
        case 7:
            q = "多选题";
            break;
        case 8:
            q = "公文写作";
            break;
        case 9:
            q = "复杂填空";
            break;

    }
%>
<%@include file="/NavMenu.jsp" %>
<div class="tpl-portlet-components" style="float: left;margin-left: 2%;width: 76%">
    <div class="portlet-title">
        <div class="caption font-green bold">
            <span class="am-icon-code"></span> 题目编辑
        </div>
    </div>
    <div class="tpl-block">
        <form action="/tikuManage/UpdateTimu" method="post" name="changeCDForm" >
            <table class="am-table am-table-bordered" style="width: 100%;" border="solid">
                <tr>
                    <th class="head" colspan="5">题目详细信息</th>
                </tr>
                <tr>
                    <th>课程</th>
                    <td colspan="4">${courseName}</td>
                </tr>
                <tr>
                    <th>题型</th>
                    <td colspan="4"><%=q%>
                    </td>
                </tr>
                <tr style="display:none">
                    <td><input style="" type="hidden" name="type" id="kind" value=${kind}></td>
                </tr>
                <tr style="display:none">
                    <c:choose>
                        <c:when test="${not empty timuId}">
                            <td><input  type="text" name="timuId"  value='${timuId}'></td>
                        </c:when>
                        <c:otherwise>
                            <td><input  type="text" name="timuId"  value='-1'></td>
                        </c:otherwise>
                    </c:choose>
                </tr>

                <tr style="display:none">
                    <td><input  type="text" name="Timu.course_id"  value='${courseId}'></td>
                </tr>
                <!--简答题 资料分析 公文写作 阅读选择-->
                <c:choose>
                    <c:when test="${kind eq 5 or kind eq 6 or kind eq 8 or kind eq 9}">
                        <th>题干（不能为空）</th>
                        <td colspan="4">
                            <textarea  rows="5" cols="40"  id="Areabody" name="Timu.body">${Timu.body}</textarea>
                        </td>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <th>题干（不能为空）</th>
                            <td colspan="4"><input style="width:85%;" type="text"  id="body" name="Timu.body" value='${Timu.body}'></td>
                        </tr>
                    </c:otherwise>
                </c:choose>

                <!--选择题 阅读选择 多项选择-->
                <c:choose>
                    <c:when test="${kind eq 1 or kind eq 9 or kind eq 7}">
                        <th>选项（至少含有两个选项）</th>
                        <td colspan="4">
                            A&nbsp;<input style="width:20%;" type="text" size="5" id="A" name="A" value='${Timu.A}'>
                            B&nbsp;<input style="width:20%;" size="5" type="text" id="B" name="B" value='${Timu.B}'>
                            C&nbsp;<input style="width:20%;" size="5" type="text" id="C" name="C" value='${Timu.C}'>
                            D&nbsp; <input style="width:20%;" size="5" type="text" id="D" name="D" value='${Timu.D}'>
                        </td>
                    </c:when>
                </c:choose>
                <tr>
                    <th>答案（不能为空）</th>
                    <td colspan="4">
                        <input type="text" name="Timu.answer" id="answer" value="${Timu.answer}">
                        <c:choose>
                            <c:when test="${kind == 2}">
                                填入1或0,是为1,否为0
                            </c:when>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <th>信息</th>
                    <td>章节：<input type="text" name="Timu.chapter" value="${chapter}"/></td>
                    <td>难度：<input type="text" name="Timu.rate" value=""/></td>
                    <td>相似度：
                        <input type="text" name="Timu.samelevel" value="${Timu.samelevel}"/>
                        <input type="hidden" name="Timu.frequency" value="${Timu.frequency}"/>
                    </td>
                    <td>分数（不能为空）：<input type="text" name="Timu.score" id="score" value="${Timu.score}"/></td>
                </tr>
                <tr>
                    <th>所属套题（不能为空）</th>
                    <td colspan="4"><input type="text" name="Timu.taoti" id="taoti" value="${Timu.taoti}"></td>
                </tr>
                <tr>
                    <th>知识点</th>
                    <td colspan="4"><input type="text" name="Timu.point" value="${Timu.point}"></td>
                </tr>
                <tr>
                    <th>出题人</th>
                    <td colspan="4"><input type="text" name="Timu.author" value="${Timu.author}"></td>
                </tr>
                <tr>
                    <th>出题时间</th>
                    <td colspan="4">
                        <div class="am-input-group am-datepicker-date" data-am-datepicker="{format: 'dd-mm-yyyy'}">
                            <input  type="text" readonly class="am-form-field" name="createDate" value="${Timu.createDate}">
                            <span class="am-input-group-btn am-datepicker-add-on">
                            <button class="am-btn am-btn-default" type="button">
                                <span class="am-icon-calendar"></span>
                            </button>
                    </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>图片</th>
                    <td class="bg" colspan="4">
                        <input type="text" name="Timu.imagename" value="${Timu.imagename}">&nbsp;&nbsp;例:课程名拼音-章节-自定义序号.jpg(图片名不能包含汉语)
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <input type="button" value="返回" onclick="goback();" class="am-btn am-btn-primary">&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" value="添加" onclick="return check();" class="am-btn am-btn-success"/>&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="reset" value="重置" class="am-btn am-btn-danger">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<!--结果模态窗口-->
<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
    <div class="am-modal-dialog">
        <div class="am-modal-bd" id="Result">
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn">确定</span>
        </div>
    </div>
</div>
</body>
</html>

