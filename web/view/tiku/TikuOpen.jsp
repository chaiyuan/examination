<%@ page import="java.util.List" %>
<%@ page import="com.bean.openKemu" %><%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/5/26 0026
  Time: 20:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/echarts.min.js"></script>
    <script src="/assets/js/jquery.min.js"></script>
    <script language="JavaScript">
        function addTime() {
            var courseId = $('#courseID option:selected').val();
            var courseName = $('#courseID option:selected').text();
            if(courseId!="--"){
                document.getElementById("courseName").innerHTML = courseName;
                document.getElementById("courseId_update").value=courseId;
                document.getElementById("flag").value = 0;
                $('#my-prompt').modal('open');
            }
        }

        function updateTime(courseId, courseName) {
            document.getElementById("flag").value = 1;
            document.getElementById("courseId_update").value = courseId;
            document.getElementById("courseName").innerHTML = courseName;
            $('#my-prompt').modal('open');
        }
        function confirmDel(id){
            document.getElementById("openTimeId").value=id;
            $('#confirmdel').modal('open');
        }
    </script>
</head>
<body>
<%@include file="/NavMenu.jsp" %>
<div class="tpl-portlet-components" style="float: left;margin-left: 2%;width: 76%">
    <div class="portlet-title">
        <div class="caption font-green bold">
            <span class="am-icon-code"></span> 题库公开
        </div>
    </div>
    <div class="tpl-block">
        <div style="float: right;width: 70%;margin-right: 10%;">
            <div style="float: right">
                <select name="courseID" id="courseID" onchange="addTime()" data-am-selected="{ btnSize: 'sm', btnStyle: 'secondary'}">
                    <option value="--" selected>设置开放时间</option>
                    <c:forEach var="kemu" items="${kemuList}">
                        <option value="${kemu.id}">${kemu.name}</option>
                    </c:forEach>
                </select>
            </div>
            <br/>
            <br/>
            <div>
                <table class="am-table am-table-bordered am-table-radius am-table-hover" id="kemu_table">
                    <tr class="am-primary">
                        <td class="head" colspan="6" align="center">开放时间</td>
                    </tr>
                    <tr class="am-primary">
                        <td>id</td>
                        <td>科目</td>
                        <td>开始时间</td>
                        <td>结束时间</td>
                        <td>编辑</td>
                        <td>删除</td>
                    </tr>
                    <c:forEach var="openKemu" items="${openTimeList}" varStatus="varstatus">
                        <tr id="${varstatus.index}+2">
                            <td>${openKemu.openTimeModel.id}</td>
                            <td>${openKemu.courseModel.name}</td>
                            <td>${openKemu.openTimeModel.beginTime}</td>
                            <td>${openKemu.openTimeModel.endTime}</td>
                            <td width="8%" style="text-align:center;">
                                <img src="/assets/img/tekunxinxi.gif" border="0" title="修改操作" id="update_btn"
                                     onClick="updateTime(${openKemu.courseModel.id},'${openKemu.courseModel.name}')"/>
                            </td>
                            <td width="8%" style="text-align:center;"><img src="/assets/img/delete.gif" id="" onclick="confirmDel(${openKemu.openTimeModel.id})"
                                                                           border="0" title="删除操作"/>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
        <!--设置开放时间模态窗口-->
        <div class="am-modal am-modal-prompt" tabindex="-1" id="my-prompt">
            <div class="am-modal-dialog">
                <div class="am-modal-hd" id="courseName"></div>
                <!--0为新增。1为修改-->
                <input id="flag" name="flag" type="hidden" value="" readonly>
                <input id="courseId_update" name="courseId" type="hidden" value="" readonly/>
                <table width="90%">
                    <tr>
                        <td align="right" width="20%">开放时间：</td>
                        <td>
                            <div class="am-input-group am-datepicker-date" data-am-datepicker="{format: 'dd-mm-yyyy'}">
                                <input type="text" class="am-form-field" name="createDate" value="" id="beginTime"
                                       readonly>
                                <span class="am-input-group-btn am-datepicker-add-on">
                                 <button class="am-btn am-btn-default" type="button">
                                     <span class="am-icon-calendar"></span>
                                </button>
                                </span>
                            </div>
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="20%">结束时间：</td>
                        <td>
                            <div class="am-input-group am-datepicker-date" data-am-datepicker="{format: 'dd-mm-yyyy'}">
                                <input type="text" class="am-form-field" name="createDate" value="" id="endTime"
                                       readonly>
                                <span class="am-input-group-btn am-datepicker-add-on">
                                 <button class="am-btn am-btn-default" type="button">
                                     <span class="am-icon-calendar"></span>
                                </button>
                                </span>
                            </div>
                        </td>
                    </tr>
                </table>

                <div class="am-modal-footer">
                    <span class="am-modal-btn" data-am-modal-cancel>取消</span>
                    <span class="am-modal-btn" id="submit_btn">提交</span>
                </div>
            </div>
        </div>
        <!--删除模态窗口-->
        <div class="am-modal am-modal-confirm" tabindex="-1" id="my-confirm">
            <div class="am-modal-dialog">
                <div class="am-modal-bd">
                    你，确定要删除这条记录吗？
                </div>
                <div class="am-modal-footer">
                    <span class="am-modal-btn" data-am-modal-cancel>取消</span>
                    <span class="am-modal-btn" data-am-modal-confirm>确定</span>
                </div>
            </div>
        </div>
        <!--结果模态窗口-->
        <div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
            <div class="am-modal-dialog">
                <div class="am-modal-bd" id="message">
                </div>
                <div class="am-modal-footer">
                    <span class="am-modal-btn" id="confirm_btn">确定</span>
                </div>
            </div>
        </div>
        <!--确认删除模态窗口-->
        <div class="am-modal am-modal-confirm" tabindex="-1" id="confirmdel">
            <div class="am-modal-dialog">
                <input type="hidden" id="openTimeId" value="">
                <div class="am-modal-bd">
                    你，确定要删除这条记录吗？
                </div>
                <div class="am-modal-footer">
                    <span class="am-modal-btn" data-am-modal-cancel>取消</span>
                    <span id="confirmdel_btn" class="am-modal-btn" >确定</span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $(function(){
        $('#submit_btn').on('click',function(){
            var courseId = document.getElementById("courseId_update").value;
            var beginTime = document.getElementById("beginTime").value;
            var endTime = document.getElementById("endTime").value;
            var flag=document.getElementById("flag").value;
            if(beginTime.length>0&&endTime.length>0){
                if(beginTime<endTime){
                    $.ajax({
                        method: "post",
                        url: "/tikuManage/openTime",
                        data: {
                            "courseId": courseId,
                            "beginTime": beginTime,
                            "endTime": endTime,
                            "flag":flag
                        },
                        success: function (res) {
                            document.getElementById("message").innerHTML=res;
                            $('#my-alert').modal('open');
                        },

                    });
                }else{
                    document.getElementById("message").innerHTML="开始时间不能大于结束时间";
                    $('#my-alert').modal('open');

                }
            }else{
                document.getElementById("message").innerHTML="开始时间与结束时间不能为空";
                $('#my-alert').modal('open');
            }

        });
        $('#confirm_btn').on('click',function(){
                window.location.href="findKemu";
        });
        $('#confirmdel_btn').on('click',function(){
            var openTimeId=document.getElementById("openTimeId").value;
                $.ajax({
                    method:"post",
                    data:{
                        "openTimeId":openTimeId
                    },
                    url:"/tikuManage/deleteOpenTime",
                    success: function(res){
                        document.getElementById("message").innerHTML=res;
                        $('#my-alert').modal('open');
                    }
                });
        });
    });

</script>

</body>
</html>
