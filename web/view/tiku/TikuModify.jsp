<%@ page import="com.utils.ConstData" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/5/26 0026
  Time: 20:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/echarts.min.js"></script>
    <script language="JavaScript">
        function addTimu() {
            var courseId = $('#course option:selected').val();
            var courseName = $('#course option:selected').text();
            var kind = $('#type option:selected').val();
            var chapter = $('#chapter option:selected').val();
            var url = "/tikuManage/findTimu?courseId=" + courseId + "&courseName=" + courseName + "&kind=" + kind + "&chapter=" + chapter;
            url = encodeURI(encodeURI(url));
            window.location.href = url;
        }
        function ref() {
            window.location.href = "/tikuManage/findTiku";
        }

        function updateTimu(id) {
            var courseId = $('#course option:selected').val();
            var courseName = $('#course option:selected').text();
            var kind = $('#type option:selected').val();
            var url = "/tikuManage/findTimu?courseId=" + courseId + "&courseName=" + courseName + "&kind=" + kind + "&timuId=" + id + "&chapter=" + "${chapter}" + "&page=" + "${page}";
            url = encodeURI(encodeURI(url));
            window.location.href = url;
        }

        function  deleteTiku(id) {
            $('#delete_id').val(id);
            $('#delete-confirm').modal('open');
        }
    </script>
</head>
<body>
<%@include file="/NavMenu.jsp" %>
<div class="tpl-portlet-components" style="float: left;margin-left: 2%;width: 76%">
    <div class="portlet-title">
        <div class="caption font-green bold">
            <span class="am-icon-code"></span> 题库修改
        </div>
    </div>
    <div class="tpl-block">
        <div style="width: 100%;margin-left: 10%;">
            <form action="/tikuManage/findTiku" method="post">
                <table class="am-table am-table-bordered am-table-radius am-table-striped" style="width: 80%;"
                       border="solid">
                    <tr align="center">
                        <td>科目
                            <select name="course_id" id="course"
                                    data-am-selected="{btnWidth: '18%', btnSize: 'sm', btnStyle: 'primary'}">
                                <option value="${course.id}" selected="selected">${course.name}</option>
                                <c:forEach var="course" items="${courseList}">
                                    <option value="${course.id}">${course.name}</option>

                                </c:forEach>
                            </select>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            题型
                            <select name="type" id="type"
                                    data-am-selected="{ btnWidth: '15%',btnSize: 'sm', btnStyle: 'primary'}">
                                <option selected="selected" value="${timuType}">${timutype1}</option>
                                <%
                                    ConstData data = new ConstData();
                                    Map map = data.name_Map;
                                    Iterator it = map.keySet().iterator();
                                    while (it.hasNext()) {
                                        Integer key = (Integer) it.next();
                                %>
                                <option value="<%=key.intValue()%>"><%=map.get(key)%><%=key.intValue()%>
                                </option>
                                <%
                                    }
                                %>
                            </select>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            章节
                            <select name="chapter" id="chapter"
                                    data-am-selected="{ btnWidth: '15%',btnSize: 'sm', btnStyle: 'primary'}">
                                <option value="${chapter}" selected="selected">${chapter}</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="0">All</option>
                            </select>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="submit" value="查询 " onclick=""
                                   class="am-btn am-btn-default am-btn-xg am-btn-primary"/>
                        </td>
                        <td><input type="button" value="增加试题 " onclick="addTimu()"
                                   class="am-btn am-btn-default am-btn-xg am-btn-primary" id="addTikuButton"/></td>
                    </tr>
                </table>
            </form>
            <table class="am-table am-table-bordered am-table-radius am-table-striped" style="width: 80%;">
                <c:forEach var="timu" varStatus="t" items="${Page.list}">
                    <td>第${timu.chapter}章</td>
                    <td>题目：${timu.body}</td>
                    <td>出题人：${timu.author}</td>
                    <td><input type="button" value="详情" onclick="updateTimu(${timu.id})"
                               class="am-btn am-btn-default am-btn-xg am-btn-primary am-round"></td>
                    <td><input type="button" value="删除" onclick="deleteTiku(${timu.id})"
                               class="am-btn am-btn-default am-btn-xg am-btn-danger am-round"></td>
                    </tr>
                </c:forEach>
            </table>
            <ul class="am-pagination am-pagination-centered">
                <c:choose>
                    <c:when test="${Page.totalPage>0}">
                        <li><a href="${pageurl}&page=1">共${Page.totalPage}页</a></li>
                        <li><a href="${pageurl}&page=1">首页</a></li>
                        <c:choose>
                            <c:when test="${Page.pageNumber >1}">
                                <li><a href="${pageurl}&page=${Page.pageNumber-1}">&laquo;</a></li>
                            </c:when>
                        </c:choose>

                        <li class="am-active"><a href="#">${Page.pageNumber}</a></li>
                        <c:choose>
                            <c:when test="${Page.pageNumber <Page.totalPage}">
                                <li><a href="${pageurl}&page=${Page.pageNumber+1}">&raquo;</a></li>
                            </c:when>
                        </c:choose>

                        <li><a href="${pageurl}&page=${Page.totalPage}">尾页</a></li>
                    </c:when>
                    <c:otherwise>
                        <li>当前条件无记录</li>
                    </c:otherwise>
                </c:choose>
            </ul>
            <!--显示结果的模态窗口-->
            <div class="am-modal am-modal-alert" tabindex="-1" id="ResultAlert">
                <div class="am-modal-dialog">
                    <div class="am-modal-bd" id="Result">
                    </div>
                    <div class="am-modal-footer">
                        <span class="am-modal-btn" id="confirm_btn" onclick="ref()">确定</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--删除题库模态窗口-->
<div class="am-modal am-modal-confirm" tabindex="-1" id="delete-confirm">
    <div class="am-modal-dialog">
        <div class="am-modal-bd">
            确定要删除该考题信息吗？
        </div>
        <input type="hidden" value="" id="delete_id" name="id"/>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" id="delete_confirm">确定</span>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#delete_confirm').on('click', function () {
            var id = $('#delete_id').val();
            url = "deleteIn";
            data = {id: id};
            $.post(url, data, function (res) {
                $('#Result').text(res);
                $('#ResultAlert').modal(open);
            });
        });

    });
</script>
<%
    String Result = (String) request.getAttribute("Result");
    if (Result != "" && Result != null) {
%>
<script>
    document.getElementById("Result").innerHTML = "<%=Result%>";
    $('#ResultAlert').modal('open');
</script>
<%
    }
%>
</body>
</html>
