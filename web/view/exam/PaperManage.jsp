<%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/7/30 0030
  Time: 10:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/jquery.min.js"></script>
    <script>
        function findStudents() {
            var url = "/paper/findStudents";
            var stuCondition = $('#stuCondition').val();
            $.post(url, {stuCondition: stuCondition}, function (data) {
                var students = data;
                $('#studentBody').empty();
                for (var i = 0; i < students.length; i++) {
                    var html = "<label class='am-checkbox'><input type='checkbox' name='stuCheckBox' value='" + students[i].sno + "' data-am-ucheck> " + students[i].sno+"  "+students[i].name + " </label>";
                    $('#studentBody').append(html);
                }
            });
        }
        function findKemus() {
            var url = "/paper/findKemus";
            var kemuCondition = $('#kemuCondition').val();
            $.post(url, {kemuCondition: kemuCondition}, function (data) {
                var kemus = data;
                $('#kemuBody').empty();
                for (var i = 0; i < kemus.length; i++) {
                    var html = "<label class='am-checkbox'><input type='checkbox' name='kemuCheckBox' value='" + kemus[i].id + "' data-am-ucheck> " + kemus[i].name + " </label>";
                    $('#kemuBody').append(html);
                }
            });
        }
        //页面加载后默认调用findStduent()和findKemu()
        $(function () {
            findStudents();
            findKemus();
        })
    </script>
</head>
<body>
<%@ include file="/NavMenu.jsp" %>
<div style="float: left;margin-left: 2%;width: 76%">
    <div class="tpl-portlet-components">
        <div class="portlet-title">
            <div class="caption font-green bold">
                <span class="am-icon-code"></span> 试卷分配
            </div>
        </div>
        <div class="tpl-block">
            <form method="post" action="/paper/setPaper" id="submit_form">
            <div style="float: left;width:250px;margin-left:20%;padding: 5px;border: 1px black solid">
                <span class="am-input-group-label"><i class="am-icon-user am-icon-fw"></i></span>
                <input type="text" id="stuCondition" style="width: 50%" placeholder="查询考生" onblur="findStudents()">
                <input type="button" id="selectAll" class="am-btn am-btn-primary" style="width: 47%" value="全选/反选"
                       onclick="operateStuBox()">
                <div class="am-scrollable-vertical" style="height: 200px;" id="studentBody">
                </div>
            </div>

            <div style="float:left;width:250px;margin-left: 50px;padding: 5px;text-align: center;border: 1px black solid">
                <span class="am-input-group-label"><i class="am-icon-user am-icon-fw"></i></span>
                <input type="text" id="kemuCondition" style="width: 50%" placeholder="查询科目" onblur="findKemus()">
                <input type="button" id="" class="am-btn am-btn-primary" style="width: 47%" value="全选/反选"
                       onclick="operateKemuBox()">
                <div class="am-scrollable-vertical" style="height: 200px;" id="kemuBody">
                </div>
            </div>

            <div style="float: left;margin-left: 20px;">
                <input type="button" class="am-btn am-btn-success" value="确定分配" id="submit_btn"><br><br>
                <button class="am-btn am-btn-success" onclick="resetAll()">重置所有</button>
            </div>
            </form>
        </div>
        <!--结果模态窗口-->
        <div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
            <div class="am-modal-dialog">
                <div class="am-modal-bd" id="message">
                </div>
                <div class="am-modal-footer">
                    <span class="am-modal-btn" id="alert_cfm">确定</span>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //学生复选框的全选和反选
    function operateStuBox() {
        $("[name='stuCheckBox']:input").each(function () {
            $(this).prop("checked", !$(this).prop("checked"));
        });
    }
    function operateKemuBox() {
        $("[name='kemuCheckBox']:input").each(function () {
            $(this).prop("checked", !$(this).prop("checked"));
        });
    }
    function resetAll(){
        $("[name='kemuCheckBox']:input").each(function () {
            $(this).removeAttr("checked");
        });
        $("[name='stuCheckBox']:input").each(function () {
            $(this).removeAttr("checked");
        });
    }
    //提交
    $('#submit_btn').on('click',function () {
        var kemus=[];
        $("input[name='kemuCheckBox']:checked").each(function(){
            kemus.push($(this).val());
        });
        var students=[];
        $("input[name='stuCheckBox']:checked").each(function(){
            students.push($(this).val());
        });
        $.post("/paper/setPaper",{stuCheckBox:students,kemuCheckBox:kemus},function(res){
            $('#message').text(res);
            $('#my-alert').modal('open');
        });
    })

    $('#alert_cfm').on('click',function () {
        findKemus();
        findStudents();
    })
</script>
</body>
</html>
