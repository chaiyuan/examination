<%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/7/3 0003
  Time: 18:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/jquery.form.js"></script>
    <script>
        //解锁,根据学生ID
        function unlock(row) {
            var operatorId = document.getElementById("tr" + row).cells[0].value;
            url = "student/unlock";
            data = {id: operatorId};
            $.post(url, data, function (res) {
                $('#message').text(res);
                $('#my-alert').modal(open);
            });
        }
        //重置密码
        function resetPwd(id) {
            $('#reset_id').val(id);
            $('#reset-confirm').modal('open');
        }

        //删除密码
        function deleteIn(id) {
            $('#delete_id').val(id);
            $('#delete-confirm').modal('open');
        }

        //修改信息
        function updateStu(id) {
            $('#update_id').val(id);
            $('#updateMoadl').modal('open');
        }


    </script>
</head>
<body>

<%@ include file="/NavMenu.jsp" %>
<div style="float: left;margin-left: 2%;width: 76%">
    <div class="tpl-portlet-components">
        <div class="portlet-title">
            <div class="caption font-green bold">
                <span class="am-icon-code"></span> 考生设置
            </div>
        </div>
        <div class="tpl-block">
            <div style="float: right">
                <button type="button" class="am-btn am-btn-primary" id="download_btn"
                        onclick="window.location.href='downloadMuban'">下载模版
                </button>
                <button type="button" class="am-btn am-btn-secondary" id="import_btn"
                        onclick="$('#importModal').modal(open);">导入考生
                </button>
                <button type="button" class="am-btn am-btn-success" id="add_btn"
                        onclick="$('#addStuModal').modal('open')">手动添加
                </button>
                <button type="button" class="am-btn am-btn-success" id="check_btn"
                        onclick="$('#checkStuModal').modal('open')">手动查询
                </button>
            </div>
            <br>
            <br>
            <div>
                <table class="am-table am-table-bordered am-table-hover">
                    <tr class="am-primary">
                        <td colspan="10" align="center">考生列表</td>
                    </tr>
                    <tr align="center">
                        <td>ID</td>
                        <td>考生学校</td>
                        <td>考生考号</td>
                        <td>考生姓名</td>
                        <td>年级</td>
                        <td>提交解锁</td>
                        <td>修改</td>
                        <td>重置密码</td>
                        <td>删除</td>
                    </tr>
                    <c:forEach items="${Page.list}" varStatus="num" var="stu">
                        <tr id="tr${num.index}" align="center">
                            <td width="4%">${stu.id}</td>
                            <td width="10%">${stu.school}</td>
                            <td width="10%">${stu.sno}</td>
                            <td width="4%" id="op">${stu.name}</td>
                            <td width="7%">${stu.sclass}</td>
                            <td width="5%">
                                <img src="/assets/img/unlock.png" border="0px" title="解锁"
                                     onclick="unlock(${num.index})"/>
                            </td>
                            <td width="8%" style="text-align: center;" id="updata_btn"><img src="/assets/img/update.png" border="0"
                                                                                            title="修改操作" onClick="updateStu(${stu.id})"/></td>
                            <td width="5%" >
                                <img src="/assets/img/reset.png" border="0" title="重置" onclick="resetPwd(${stu.id})"/>
                            </td>
                            <td width="5%" style="text-align: center;" id="delete_btn"><img src="/assets/img/delete.png" border="0"
                                                                                            title="删除操作" onClick="deleteIn(${stu.id})"/>
                            </td>
                        </tr>
                    </c:forEach>

                </table>
            </div>
            <ul class="am-pagination am-pagination-centered">
                <c:choose>
                    <c:when test="${Page.totalPage>0}">
                        <li><a href="findAllStu?page=1">共${Page.totalPage}页</a></li>
                        <li><a href="findAllStu?page=1">首页</a></li>
                        <c:choose>
                            <c:when test="${Page.pageNumber >1}">
                                <li><a href="findAllStu?page=${Page.pageNumber-1}">&laquo;</a></li>
                            </c:when>
                        </c:choose>

                        <li class="am-active"><a href="#">${Page.pageNumber}</a></li>
                        <c:choose>
                            <c:when test="${Page.pageNumber <Page.totalPage}">
                                <li><a href="findAllStu?page=${Page.pageNumber+1}">&raquo;</a></li>
                            </c:when>
                        </c:choose>

                        <li><a href="findAllStu?page=${Page.totalPage}">尾页</a></li>
                    </c:when>
                    <c:otherwise>
                        <li>当前条件无记录</li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
    </div>
</div>

<!--结果模态窗口-->
<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
    <div class="am-modal-dialog">
        <div class="am-modal-bd" id="message">
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" id="alert_btn">确定</span>
        </div>
    </div>
</div>

<!--重置密码模态窗口-->
<div class="am-modal am-modal-confirm" tabindex="-1" id="reset-confirm">
    <div class="am-modal-dialog">
        <div class="am-modal-bd">
            确定要重置该学生密码吗？
        </div>
        <input type="hidden" value="" id="reset_id" name="id"/>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" id="reset_confirm">确定</span>
        </div>
    </div>
</div>


<!--删除学生模态窗口-->
<div class="am-modal am-modal-confirm" tabindex="-1" id="delete-confirm">
    <div class="am-modal-dialog">
        <div class="am-modal-bd">
            确定要删除该学生信息吗？
        </div>
        <input type="hidden" value="${stu.id}" id="delete_id" name="id"/>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" id="delete_confirm">确定</span>
        </div>
    </div>
</div>
<!--导入考生模态窗口-->
<div class="am-modal am-modal-confirm" tabindex="-1" id="importModal">
    <div class="am-modal-dialog">
        <div class="am-modal-bd" align="center">
            请选择导入考生的xls表格文件
            <form id="importForm" method="post" enctype="multipart/form-data">
                <br>
                <input type="file" name="StudentsList" id="import_file" style="margin-left: 35%"/>
            </form>
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" id="import_confirm">确定</span>
        </div>
    </div>
</div>
<!--手动添加考生模态窗口-->
<div class="am-modal am-modal-confirm" tabindex="-1" id="addStuModal">
    <div class="am-modal-dialog">
        <div class="am-modal-bd" align="center">
            <p>手动添加考生</p>
            <form id="addStuForm" method="post">
                <table align="center" border="1" class="am-table am-table-bordered am-table-radius am-table-striped">
                    <tr>
                        <td>考生姓名</td>
                        <td><input id="addStuName" type="text" onblur="isNull(this)"
                                   name="addStuName"/></td>
                    </tr>
                    <tr>
                        <td>考生考号</td>
                        <td><input id="addStuNo" type="text" onblur="isNull(this)"
                                   name="addStuNo" value=""/></td>
                    </tr>
                    <tr>
                        <td>学校</td>
                        <td><input id="addStuSchool" type="text" onblur="isNull(this)"
                                   name="addStuSchool"/></td>
                    </tr>
                    <tr>
                        <td>年级</td>
                        <td><input id="addStuScalss" type="text" onblur="isNull(this)"
                                   name="addStuScalss"/></td>
                    </tr>
                    <tr>
                        <td>操作</td>
                        <td colspan="3"><input type="button" name="A1"
                                               value="添加" class="am-btn am-btn-primary" onclick="addStudent()"/>
                            <input type="reset" value="清空" class="am-btn am-btn-danger"/>
                        </td>
                    </tr>
                </table>

            </form>
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" id="add_confirm" onclick="addStudent()">确定</span>
        </div>
    </div>
</div>
<!--手动查询-->
<div class="am-modal am-modal-confirm" tabindex="-1" id="checkStuModal">
    <div class="am-modal-dialog">
        <div class="am-modal-bd" align="center">
            <p>手动查询考生</p>
            <form id="checkStuForm" method="post" action="/student/findAllStu">
                <table align="center" border="1" class="am-table am-table-bordered am-table-radius am-table-striped">
                    <tr>
                        <td>考生姓名</td>
                        <td><input id="checkStuName" type="text" name="addStuName"/></td>
                    </tr>
                    <tr>
                        <td>考生考号</td>
                        <td><input id="checkStuNo" type="text"   name="addStuNo" value=""/></td>
                    </tr>
                    <tr>
                        <td>学校</td>
                        <td><input id="checkStuSchool" type="text"   name="addStuSchool"/></td>
                    </tr>
                    <tr>
                        <td>年级</td>
                        <td><input id="checkStuScalss" type="text"  name="addStuScalss"/></td>
                    </tr>
                    <tr>
                        <td>操作</td>
                        <td colspan="3"><input type="submit" name="A1"
                                               value="查询" class="am-btn am-btn-primary" />
                            <input type="reset" value="清空" class="am-btn am-btn-danger"/>
                        </td>
                    </tr>
                </table>

            </form>
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" id="check_confirm" >确定</span>
        </div>
    </div>
</div>
<!--修改考生模态窗口-->
<div class="am-modal am-modal-confirm" tabindex="-1" id="updateMoadl">
    <div class="am-modal-dialog">
        <div class="am-modal-bd" align="center">
            <p>修改学生信息</p>
            <form id="updateForm">
                <table align="center" border="1" class="am-table am-table-bordered am-table-radius am-table-striped">

                    <tr>
                        <td><input type="hidden" value="${stu.id}" id="update_id" name="id"/></td>
                    </tr>
                    <tr>
                        <td>考生姓名</td>
                        <td><input id="updateStuName" type="text" onblur="isNull(this)"
                                   name="updateStuName"/></td>
                    </tr>
                    <tr>
                        <td>考生考号</td>
                        <td><input id="updateStuNo" type="text" onblur="isNull(this)"
                                   name="updateStuNo" value=""/></td>
                    </tr>
                    <tr>
                        <td>学校</td>
                        <td><input id="updateStuSchool" type="text" onblur="isNull(this)"
                                   name="updateStuSchool"/></td>
                    </tr>
                    <tr>
                        <td>年级</td>
                        <td><input id="updateStuScalss" type="text" onblur="isNull(this)"
                                   name="updateStuScalss"/></td>
                    </tr>
                </table>

            </form>
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>取消</span>
            <span class="am-modal-btn" id="update_confirm" onclick="updateStudent()">确定</span>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#reset_confirm').on('click', function () {
            var id = $('#reset_id').val();
            url = "resetPwd";
            data = {id: id};
            $.post(url, data, function (res) {
                $('#message').text(res);
                $('#my-alert').modal(open);
            });
        });
        $('#import_confirm').on('click', function () {
            var file = $('#import_file').val();
            if (file != null) {
                var options = {
                    url: "importStudents",
                    method: "post",
                    success: function (res) {
                        $('#importModal').modal('close');
                        alert(res);
                    },
                    error: function (res) {
                        $('#importModal').modal('close');
                        alert(res);
                    }
                };
                //利用jquery.form.js提交表单
                $('#importForm').ajaxSubmit(options);
            }
        });

        $('#alert_btn').on('click', function () {
            window.location.href = "findAllStu";
        });

        $(function () {
            $('#delete_confirm').on('click', function () {
                var id = $('#delete_id').val();
                url = "deleteIn";
                data = {id: id};
                $.post(url, data, function (res) {
                    $('#message').text(res);
                    $('#my-alert').modal(open);
                });
            });

        });



        $('#alert_btn').on('click', function () {
            window.location.href = "findAllStu";
        });
    });


    function addStudent() {
        $.ajax({
            method: "post",
            url:"addStudent",
            data:$('#addStuForm').serialize(),
            success:function (res) {
                alert(res);
            }
        });
    }

    function updateStudent() {
        $.ajax({
            method: "post",
            url:"updateStudent",
            data:$('#updateForm').serialize(),
            success:function (res) {
                $('#message').text(res);
                $('#my-alert').modal(open);
            }
        });
    }

    //判断输入框是否为空
    function isNull(input){
        var value=input.value.trim();
        if(value==""){
            input.style['border-color']="red";
        }else{
            input.style['border-color']="green";
        }
    }
</script>
</body>
</html>
