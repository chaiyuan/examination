<%--
  Created by IntelliJ IDEA.
  User: Hexun
  Date: 2017/7/27 0027
  Time: 16:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <script src="/assets/js/jquery.min.js"></script>
    <script>

    </script>
</head>
<body>
<%@ include file="/NavMenu.jsp" %>
<div style="float: left;margin-left: 2%;width: 76%">
    <div class="tpl-portlet-components">
        <div class="portlet-title">
            <div class="caption font-green bold">
                <span class="am-icon-code"></span> 试卷分配
            </div>
        </div>
        <div class="tpl-block">
            <div style="width: 100%;">
                <div style="width: 68%;float: left"><table width="100%" class="am-table am-table-bordered am-table-hover">
                    <tr align="center" bgcolor="#87cefa">
                        <td colspan="4">已生成的母卷</td>
                    </tr>
                    <tr align="center">
                        <td width="30%">科目</td>
                        <td>母卷编号</td>
                        <td>时间</td>
                        <td>操作</td>
                    </tr>
                    <c:forEach var="mujuan" items="${MujuanList}">
                        <tr align="center">
                            <td width="30%">${mujuan.name}</td>
                            <td>${mujuan.id}</td>
                            <c:choose>
                                <c:when test="${mujuan.exam_time eq 0}">
                                    <td><input type="text" name="" id="a" value=""></td>
                                    <td>
                                        <button class="am-btn am-btn-primary">添加至当前考试</button>
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td><input type="text" name="" id="b" value="${mujuan.exam_time}"
                                               style="background: #FFF8DC;"></td>
                                    <td>
                                        <button class="am-btn am-btn-success">修改当前考试时间</button>
                                    </td>
                                </c:otherwise>
                            </c:choose>
                        </tr>
                    </c:forEach>
                </table></div>
                <div style="width: 30%;float: right"> <table width="100%" class="am-table am-table-bordered am-table-hover">
                    <tr align="center" bgcolor="#87cefa">
                        <td class="head" colspan="3">当前的考试</td>
                    </tr>
                    <tr align="center">
                        <td>科目</td>
                        <td>场次</td>
                        <td>时间(分钟)</td>
                    </tr>
                    <tr align="center">
                        <td>科目</td>
                        <td>场次</td>
                        <td>时间(分钟)</td>
                    </tr>
                </table></div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
