<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>体校考试系统</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
    <link rel="stylesheet" href="assets/css/admin.css">
    <link rel="stylesheet" href="assets/css/app.css">
    <script src="assets/js/echarts.min.js"></script>
</head>

<body data-type="index">
<!--菜单栏及首页在NavMenu.jsp中 -->
<%@include file="/NavMenu.jsp"%>
<div class="am-u-md-6 am-u-sm-12 row-mb">
    <div class="tpl-portlet">
        <div class="tpl-portlet-title">
            <div class="tpl-caption font-red ">
                <i class="am-icon-bar-chart"></i>
                <span> Cloud 动态资料</span>
            </div>
            <div class="actions">
                <ul class="actions-btn">
                    <li class="purple-on">昨天</li>
                    <li class="green">前天</li>
                    <li class="dark">本周</li>
                </ul>
            </div>
        </div>
        <div class="tpl-scrollable">
            <div class="number-stats">
                <div class="stat-number am-fl am-u-md-6">
                    <div class="title am-text-right"> Total</div>
                    <div class="number am-text-right am-text-warning"> 2460</div>
                </div>
                <div class="stat-number am-fr am-u-md-6">
                    <div class="title"> Total</div>
                    <div class="number am-text-success"> 2460</div>
                </div>

            </div>

            <table class="am-table tpl-table">
                <thead>
                <tr class="tpl-table-uppercase">
                    <th>人员</th>
                    <th>余额</th>
                    <th>次数</th>
                    <th>效率</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <img src="assets/img/user01.png" alt="" class="user-pic">
                        <a class="user-name" href="">禁言小张</a>
                    </td>
                    <td>￥3213</td>
                    <td>65</td>
                    <td class="font-green bold">26%</td>
                </tr>
                <tr>
                    <td>
                        <img src="assets/img/user02.png" alt="" class="user-pic">
                        <a class="user-name" href="">Alex.</a>
                    </td>
                    <td>￥2635</td>
                    <td>52</td>
                    <td class="font-green bold">32%</td>
                </tr>
                <tr>
                    <td>
                        <img src="assets/img/user03.png" alt="" class="user-pic">
                        <a class="user-name" href="">Tinker404</a>
                    </td>
                    <td>￥1267</td>
                    <td>65</td>
                    <td class="font-green bold">51%</td>
                </tr>
                <tr>
                    <td>
                        <img src="assets/img/user04.png" alt="" class="user-pic">
                        <a class="user-name" href="">Arron.y</a>
                    </td>
                    <td>￥657</td>
                    <td>65</td>
                    <td class="font-green bold">73%</td>
                </tr>
                <tr>
                    <td>
                        <img src="assets/img/user05.png" alt="" class="user-pic">
                        <a class="user-name" href="">Yves</a>
                    </td>
                    <td>￥3907</td>
                    <td>65</td>
                    <td class="font-green bold">12%</td>
                </tr>
                <tr>
                    <td>
                        <img src="assets/img/user06.png" alt="" class="user-pic">
                        <a class="user-name" href="">小黄鸡</a>
                    </td>
                    <td>￥900</td>
                    <td>65</td>
                    <td class="font-green bold">10%</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>

</html>