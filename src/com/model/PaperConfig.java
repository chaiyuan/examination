package com.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Hexun on 2017/6/22 0022.
 */
public class PaperConfig extends Model<PaperConfig> {
    public static PaperConfig dao = new PaperConfig();
    public Course getCourse() {
        return Course.dao.findById(get("course_id").toString());
    }
}
