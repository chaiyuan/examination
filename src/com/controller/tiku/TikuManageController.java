package com.controller.tiku;

import com.bean.openKemu;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.model.*;
import com.utils.ConstData;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Hexun on 2017/6/5 0005.
 */
public class TikuManageController extends Controller{
    /**
    *@Author:HeXun
    *@Description: 查找科目列表和科目开放列表,返回科目列表和开放的科目列表
    *@Date: 2017/6/18 0018
    */
    public void findKemu(){
        //查询没有设置开放时间的科目列表
        setAttr("kemuList",Course.dao.find("select * from courses where id not in (select courseId from opentime)"));
        List<OpenTime> op=OpenTime.dao.find("select * from opentime");
        List<openKemu> list=new ArrayList<openKemu>();
        for(OpenTime o:op){
            openKemu openkemu=new openKemu();
            openkemu.setCourseModel(Course.dao.findById(o.get("courseId").toString()));
            openkemu.setOpenTimeModel(o);
            list.add(openkemu);
        }
        setAttr("openTimeList",list);
        render("/view/tiku/TikuOpen.jsp");
    }
    /**
    *@Author: HeXun
    *@Description :查找题库
    *@param :Page当前页数 默认为1 chapter当前章节 默认为全部 course_id科目id  默认为数据库中第一条记录 type题目类型 默认为单选题CHOICE
    *@Date:2017/6/6 0006
    */
    public void findTiku(){
        int page=1;
        int chapter=0;
        int type= ConstData.CHOICE;
        int course_id= 11;
        if(getPara("course_id")!=null&&!"".equals(getPara("course_id"))){
            course_id=getParaToInt("course_id");
        }
        try{
            if(getPara("page")!=null){
                page=Integer.parseInt(getPara("page"));
            }
            if(getPara("chapter")!=null){
                chapter=Integer.parseInt(getPara("chapter"));
            }
            if(getPara("type")!=null){
                type=Integer.parseInt(getPara("type"));
            }
            if(getPara("course")!=null){
                course_id=Integer.parseInt(getPara("course"));
            }
            String timuType=null;
            switch (type){
                case ConstData.CHOICE:timuType="choice";break;
                case ConstData.JUDGE:timuType="judge";break;
                case ConstData.SIMPLE_FILL:timuType="simplefill";break;
                case ConstData.COMPLEX_FILL:timuType="complexfill";break;
                case ConstData.SIMPlE_PRESS:timuType="simplepress";break;
                case ConstData.PROGRAM:timuType="program";break;
                case ConstData.CHOICE_FILL:timuType="choicefill";break;
                case ConstData.GONGWENXIEZUO:timuType="gongwenxiezuo";break;
                case ConstData.CHOICEREAD:timuType="choiceread";break;
            }
            if(chapter!=0){
                setAttr("Page",Db.paginate(page,ConstData.SIZE,"select *","from "+timuType+" where course_id="+course_id+" and chapter="+chapter+" order by id"));
            }else{
                setAttr("Page",Db.paginate(page,ConstData.SIZE,"select *","from "+timuType+" where course_id="+course_id));//"from"+timuType+" where course_id="+course_id+" order by id"
            }
        }catch(Exception e){
            setAttr("Result","输入查询条件有误，请验证后重新查询");
            e.printStackTrace();
        }
        setAttr("timutype1",ConstData.showType(type));
        setAttr("pageurl","/tikuManage/findTiku?type="+type+"&chapter="+chapter+"&course_id="+course_id);
        setAttr("course",Course.dao.findById(course_id));
        setAttr("timuType",type);
        setAttr("courseList",Course.dao.find("select * from courses"));
        setAttr("chapter",chapter);
        setAttr("page",page);
        render("../tiku/TikuModify.jsp");
    }
    /**
    *@Author: HeXun
    *@Description: 修改题目,由于不同类型题目只有是否有ABCD选项不同所以采用getModel方式接收参数。
     * @param: TimuId  题目id 默认为-1代表新增题目
    *@Date:2017/6/9 0009
    */
    public void UpdateTimu(){
        try {
            int type = getParaToInt("type");
            Date createDate=new Date();
            String strDate = getPara("createDate").trim();
            SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd");
            if ("".equals(strDate) || strDate == null) {
                String temp = format.format(createDate);
                createDate = format.parse(temp);
            } else {
                createDate = format.parse(strDate);
            }
            int TimuId=getParaToInt("timuId");
            String A,B,C,D;
            switch (type){
                case ConstData.CHOICE:
                    A=getPara("A");
                    B=getPara("B");
                    C=getPara("C");
                    D=getPara("D");
                    Choice choice=getModel(Choice.class,"Timu");
                    choice.set("A",A);
                    choice.set("B",B);
                    choice.set("C",C);
                    choice.set("D",D);
                    choice.set("createDate",createDate);
                    if(TimuId==-1){
                        choice.save();
                    }else{
                        choice.set("id",TimuId);
                        choice.update();
                    }
                    break;
                case ConstData.JUDGE:
                    Judge judge=getModel(Judge.class,"Timu");
                    judge.set("createDate",createDate);
                    if(TimuId==-1){
                        judge.save();
                    }else{
                        judge.set("id",TimuId);
                        judge.update();
                    }
                    break;
                case ConstData.SIMPLE_FILL:
                    SimpleFill simpleFill=getModel(SimpleFill.class,"Timu");
                    simpleFill.set("createDate",createDate);
                    if(TimuId==-1){
                        simpleFill.save();
                    }else{
                        simpleFill.set("id",TimuId);
                        simpleFill.update();
                    }
                    break;
                case ConstData.COMPLEX_FILL:
                    ComplexFill complexFill=getModel(ComplexFill.class,"Timu");
                    complexFill.set("createDate",createDate);
                    if(TimuId==-1){
                        complexFill.save();
                    }else{
                        complexFill.set("id",TimuId);
                        complexFill.update();
                    }
                    break;
                case ConstData.SIMPlE_PRESS:
                    SimplePress simplePress=getModel(SimplePress.class,"Timu");
                    simplePress.set("createDate",createDate);
                    if(TimuId==-1){
                        simplePress.save();
                    }else{
                        simplePress.set("id",TimuId);
                        simplePress.update();
                    }
                    break;
                case ConstData.PROGRAM:
                    Program program=getModel(Program.class,"Timu");
                    program.set("createDate",createDate);
                    if(TimuId==-1){
                        program.save();
                    }else{
                        program.set("id",TimuId);
                        program.update();
                    }
                    break;
                case ConstData.CHOICE_FILL:
                    ChoiceFill choiceFill=getModel(ChoiceFill.class,"Timu");
                    A=getPara("A");
                    B=getPara("B");
                    C=getPara("C");
                    D=getPara("D");
                    choiceFill.set("A",A);
                    choiceFill.set("B",B);
                    choiceFill.set("C",C);
                    choiceFill.set("D",D);
                    choiceFill.set("createDate",createDate);
                    if(TimuId==-1){
                        choiceFill.save();
                    }else{
                        choiceFill.set("id",TimuId);
                        choiceFill.update();
                    }
                    break;
                case ConstData.GONGWENXIEZUO:
                    Gongwenxiezuo gongwenxiezuo=getModel(Gongwenxiezuo.class,"Timu");
                    gongwenxiezuo.set("createDate",createDate);
                    if(TimuId==-1){
                        gongwenxiezuo.save();
                    }else{
                        gongwenxiezuo.set("id",TimuId);
                        gongwenxiezuo.update();
                    }
                    break;
                case ConstData.CHOICEREAD:
                    ChoiceRead choiceRead=getModel(ChoiceRead.class,"Timu");
                    A=getPara("A");
                    B=getPara("B");
                    C=getPara("C");
                    D=getPara("D");
                    choiceRead.set("A",A);
                    choiceRead.set("B",B);
                    choiceRead.set("C",C);
                    choiceRead.set("D",D);
                    choiceRead.set("createDate",createDate);
                    if(TimuId==-1){
                        choiceRead.save();
                    }else{
                        choiceRead.set("id",TimuId);
                        choiceRead.update();
                        break;
                    }
            }
            setAttr("Result","更新成功");
        } catch (ParseException e) {
            setAttr("Result","更新失败");
            e.printStackTrace();
        }
        render("/view/tiku/TikuModify.jsp");
    }

    /**
    *@Author: HeXun
    *@Description: 题目详情
    *@Date:2017/6/12 0012
    */
    public void findTimu() throws UnsupportedEncodingException {
        int type=getParaToInt("kind");
        String courseId=getPara("courseId");
        String courseName= URLDecoder.decode(getPara("courseName"),"UTF-8");
        String timuId=getPara("timuId");
        //若题目id为空则说明为增加题目，否则从数据库中查询该题
        if(timuId!=null && !"".equals(timuId)){
            String timuType="";
            switch (type){
                case ConstData.CHOICE:
                    timuType="choice";
                    Choice choice=Choice.dao.findById(timuId);
                    setAttr("Timu",choice);
                    break;
                case ConstData.JUDGE:
                    timuType="judge";
                    Judge judge=Judge.dao.findById(timuId);
                    setAttr("Timu",judge);
                    break;
                case ConstData.SIMPLE_FILL:
                    timuType="simplefill";
                    SimpleFill simpleFill=SimpleFill.dao.findById(timuId);
                    setAttr("Timu",simpleFill);
                    break;
                case ConstData.COMPLEX_FILL:
                    timuType="complexfill";
                    ComplexFill complexFill=ComplexFill.dao.findById(timuId);
                    setAttr("Timu",complexFill);
                    break;
                case ConstData.SIMPlE_PRESS:
                    timuType="simplepress";
                    SimplePress simplePress=SimplePress.dao.findById(timuId);
                    setAttr("Timu",simplePress);
                    break;
                case ConstData.PROGRAM:
                    timuType="program";
                    Program program=Program.dao.findById(timuId);
                    setAttr("Timu",program);
                    break;
                case ConstData.CHOICE_FILL:
                    timuType="choicefill";
                    ChoiceFill choicefill=ChoiceFill.dao.findById(timuId);
                    setAttr("Timu",choicefill);
                    break;
                case ConstData.GONGWENXIEZUO:
                    timuType="gongwenxiezuo";
                    Gongwenxiezuo gongwenxiezuo=Gongwenxiezuo.dao.findById(timuId);
                    setAttr("Timu",gongwenxiezuo);
                    break;
                case ConstData.CHOICEREAD:
                    timuType="choiceread";
                    ChoiceRead choiceRead=ChoiceRead.dao.findById(timuId);
                    setAttr("Timu",choiceRead);
                    break;
            }
            setAttr("timuId",timuId);
        }else{
            setAttr("chapter",getPara("chapter"));
        }
        setAttr("kind",String.valueOf(type));
        setAttr("courseId",courseId);
        setAttr("courseName",courseName);
        render("/view/tiku/TimuModify.jsp");
    }

    /**
    *@Author:HeXun
    *@Description: 更新科目开放时间
    *@Date:2017/6/19 0019
    */
    public void openTime(){
        String beginTime=getPara("beginTime");
        String endTime=getPara("endTime");
        int courseId=getParaToInt("courseId");
        String result=null;
        if(getParaToInt("flag")==0){
            //设置了表courseId唯一约束，因此不必判断是否已有该科目
            try {
                new OpenTime().set("courseId",courseId).set("beginTime",beginTime).set("endTime",endTime).save();
                result="设置成功";
            } catch (Exception e) {
                result = "设置失败，请检查是否重复设置科目";
            }
        }else{
            try {
                OpenTime.dao.findById(courseId).set("beginTime",beginTime).set("endTime",endTime).update();
                result="修改成功";
            } catch (Exception e) {
               result="修改失败";
            }
        }
        renderText(result);
    }
    /**
    *@Author: HeXun
    *@Description: 删除科目开放时间
    *@Date: 2017/6/21 0021
    */
    public void deleteOpenTime(){
        int openTimeId = getParaToInt("openTimeId");
        try {
            OpenTime.dao.deleteById(openTimeId);
            renderText("删除成功");
        } catch (Exception e) {
            renderText("删除失败");
        }
    }

    public void deleteIn(){
        String id=getPara("id");
        Choice.dao.deleteById(id);
        Choice ch=Choice.dao.findById(id);
        if(ch==null)
        {
            renderText("删除成功");
        }
        else
        {
            renderText("删除失败");
        }
    }
}
