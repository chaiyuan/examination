package com.controller;

import com.jfinal.core.Controller;

/**
 * Created by chaiyuan on 2017/5/23.
 */
public class IndexController extends Controller{
    public void index(){
     redirect("/courses/showCourses");
 }
}
