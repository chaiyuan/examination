package com.controller.mujuan;

import com.bean.AutoOriginPaperBean;
import com.bean.ZujuanBean;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.model.Course;
import com.model.OriginPaper;
import com.model.PaperConfig;
import com.service.mujuan.MujuanService;
import jxl.biff.drawing.Origin;
import sun.text.normalizer.UTF16;

import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hexun on 2017/6/22 0022.
 */
public class MujuanController extends Controller {
    MujuanService mujuanService=new MujuanService();
    /**
    *@Author:HeXun
    *@Description: 初始化母卷管理页面，返回已生成的母卷列表
    *@Date: 2017/6/22 0022
    */
    public void showMujuan(){
        setAttr("mujuanAllList",PaperConfig.dao.find("select * from paperconfig"));
        //未组卷科目列表
        setAttr("kemuList", Course.dao.find("select * from courses where id not in (select course_id from paperconfig)"));
        //已组卷科目列表
        setAttr("zujuanKemuList",Course.dao.find("select * from courses where id  in (select course_id from paperconfig)"));
        //获取一键组卷的年级
        setAttr("grades",mujuanService.getGrades());
        render("mujuan.jsp");
    }

    /**
    *@Author:HeXun
    *@Description: 为组卷一门科目作准备，获取题目信息
    *@Date: 2017/6/22 0022
    */
    public void zujuan(){
        try{
            int courseId=getParaToInt("courseId");
            //由于可提供选择只有未组卷的科目，因此不必判断是否可以组卷
            setAttr("course",Course.dao.findById(courseId));
            getSession().setAttribute("zujuanDetail",mujuanService.getZujuanDetail(courseId));
            render("addMujuan.jsp");
        }catch(Exception e){
            e.printStackTrace();
            showMujuan();
        }
    }
    /**
    *@Author:HeXun
    *@Description: 真正组卷
    *@Date: 2017/6/24 0024
    */
    public void doZujuan(){
        final List<AutoOriginPaperBean> courseDetail= (List<AutoOriginPaperBean>)getSession().getAttribute("zujuanDetail");
        final String[] giveNumbers=getParaValues("giveNumber");
        final String courseId=getPara("courseId");
        final int changci=getParaToInt("changci");
        //boolean result=mujuanService.doAutoZujuan(courseDetail,giveNumbers,courseId,changci);//组卷
        boolean result= Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                return mujuanService.doAutoZujuan(courseDetail,giveNumbers,courseId,changci);
            }
        });
        if(result){
            setAttr("result","母卷添加成功");
        }else{
            setAttr("result","母卷添加失败");
        }
        render("addMujuan.jsp");
    }

    /**
    *@Author:HeXun
    *@Description: 删除母卷
    *@Date: 2017/6/24 0024
    */
    public void deleteMujuan(){
        String mujuanId=getPara("mujuanId");
        boolean result=PaperConfig.dao.deleteById(mujuanId);
        if(result){
            renderText("删除成功");
        }else{
            renderText("删除失败");
        }
    }

    /**
    *@Author:HeXun
    *@Description: 显示修改母卷页面
    *@Date: 2017/6/25 0025
    */
    public void showModify(){
        String courseId=getPara("courseId");
        int changci=getParaToInt("changci");
        int type=getParaToInt("timuType");
        Course course=Course.dao.findById(courseId);
        List questions=mujuanService.getOriginPaperQuestions(course,changci,null,type);
        setAttr("changci",changci);
        setAttr("type",type);
        setAttr("course",course);
        setAttr("questions",questions);
        render("Modify.jsp");
    }

    /**
    *@Author:HeXun
    *@Description: 删除母卷中题目
    *@Date: 2017/6/26 0026
    */
    public void deleteTimu(){
        String ques_id=getPara("questionId");
        String origin_id=String.valueOf(Db.queryInt("select id from originpaper where ques_id=?",ques_id)) ;
        boolean result=OriginPaper.dao.deleteById(origin_id);
        if(result){
            renderText("删除成功");
        }else{
            renderText("删除失败");
        }
    }

    /**
    *@Author: HeXun
    *@Description: 显示题库中所有题型、章节、难度相等的题目
    *@Date: 2017/6/27 0027
    */
    public void showTimu(){
        String type=getPara("type");
        String rate=getPara("rate");
        String courseId=getPara("course_id");
        String changci=getPara("changci");
        String id=getPara("id");
        List questions=mujuanService.getQuestions(type,rate,courseId);
        setAttr("questions",questions);
        setAttr("type",type);
        setAttr("course",Course.dao.findById(courseId));
        setAttr("changci",changci);
        setAttr("oldId",id);
        render("replaceTimu.jsp");
    }
    /**
    *@Author: HeXun
    *@Description: 替换母卷中题目
    *@Date: 2017/6/27 0027
    */
    public void replaceTimu(){
        String id=getPara("id");//新的题目id
        String oldId=getPara("oldId");
        boolean result=OriginPaper.dao.findFirst("select * from originpaper where ques_id=?",oldId).set("ques_id",id).update();
        if(result){
            renderText("替换成功");
        }else{
            renderText("替换失败");
        }
    }

    /**
    *@Author: HeXun
    *@Description: 显示一键组卷中各科目套题
    *@Date: 2017/7/2 0002
    */
    public void simpleMake() throws java.io.UnsupportedEncodingException{
        String grade= URLDecoder.decode(getPara("grade"), "UTF-8");
        List<Course> courses=Course.dao.find("select * from courses where name like '%"+grade+"%'");
        List<PaperConfig> paperConfigs=PaperConfig.dao.find("select * from paperconfig");
        boolean flag=true;//判断是否已组卷
        for(PaperConfig p:paperConfigs){
            for(Course c:courses){
                if(p.get("course_id").equals(c.get("id"))){
                    flag=false;
                    renderText("本年级某科目已存在母卷，请先删除母卷");
                    break;
                }
            }
        }
        if(flag){
            ZujuanBean zujuanBean=new ZujuanBean();
            for(Course c:courses){
                if((grade+"-语文").equals(c.get("name"))){
                    zujuanBean.setYuwen(c);
                    zujuanBean.setYuwenList(mujuanService.getTaoti((int)c.get("id")));
                }else if((grade+"-数学").equals(c.get("name"))){
                    zujuanBean.setShuxue(c);
                    zujuanBean.setShuxueList(mujuanService.getTaoti((int)c.get("id")));
                }else if((grade+"-政治").equals(c.get("name"))){
                    zujuanBean.setZhengzhi(c);
                    zujuanBean.setZhengzhiList(mujuanService.getTaoti((int)c.get("id")));
                }
            }
            renderJson(zujuanBean);
        }

    }

    /**
    *@Author: HeXun
    *@Description: 一键组卷
    *@Date: 2017/7/2 0002
    */
    public void doSimpleZujuan(){
        final int yuwenid = Integer.parseInt(getPara("yuwenId"));
        final int shuxueid = Integer.parseInt(getPara("shuxueId"));
        final int zhengzhiid = Integer.parseInt(getPara("zhengzhiId"));
        final int yuwenChapter = Integer.parseInt(getPara("yuwen"));
        final int shuxueChapter = Integer.parseInt(getPara("shuxue"));
        final int zhengzhiChapter = Integer.parseInt(getPara("zhengzhi"));
        boolean result=Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                return mujuanService.simpleZujuan(yuwenid,yuwenChapter)&&mujuanService.simpleZujuan(shuxueid,shuxueChapter)&&mujuanService.simpleZujuan(zhengzhiid,zhengzhiid);
            }
        });
        if(result){
            renderText("组卷成功");
        }else{
            renderText("组卷失败");
        }
    }
}
