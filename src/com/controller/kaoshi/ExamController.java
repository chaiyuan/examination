package com.controller.kaoshi;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import com.model.Course;
import com.model.PaperConfig;
import com.model.Student;

import java.util.List;

/**
 * Created by Hexun on 2017/7/27 0027.
 */
public class ExamController extends Controller{
    /**
    *@Author:HeXun
    *@Description: 展示考试管理页面
    *@Date: 2017/7/27 0027
    */
    public void show(){
        List<Record> mujuanList= Db.find("select paperconfig.*,courses.name from paperconfig,courses where paperconfig.course_id=courses.id order by exam_time desc");
        setAttr("MujuanList",mujuanList);
        render("ExamManage.jsp");
    }


}
