package com.controller.kaoshi;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.model.Student;
import com.service.kaoshi.StudentService;
import com.utils.ConstData;
import com.utils.Md5Util;

import java.io.File;

/**
 * Created by Hexun on 2017/7/25 0025.
 */
public class StudentController extends Controller {

    /**
     * @Author:HeXun
     * @Description: 查询所有考生信息
     * @Date: 2017/7/25 0025
     */
    public void findAllStu() {
        int page = getParaToInt("page") == null ? 1 : getParaToInt("page");
        String sqlExceptSelect="from student where 1=1";
        String sno=getPara("addStuNo");
        String name=getPara("addStuName");
        String school=getPara("addStuSchool");
        String sclass=getPara("addStuScalss");
        sqlExceptSelect=sno==null||"".equals(sno)?sqlExceptSelect:sqlExceptSelect+" and sno='"+sno+"'";
        sqlExceptSelect=name==null||"".equals(name)?sqlExceptSelect:sqlExceptSelect+" and name='"+name+"'";
        sqlExceptSelect=school==null||"".equals(school)?sqlExceptSelect:sqlExceptSelect+" and school='"+school+"'";
        sqlExceptSelect=sclass==null||"".equals(sclass)?sqlExceptSelect:sqlExceptSelect+" and sclass='"+sclass+"'";
        Page<Student> stuList=Student.dao.paginate(page, ConstData.SIZE,"select * ",sqlExceptSelect);
        setAttr("Page", stuList);
        render("/view/exam/StudentManage.jsp");
    }
    /**
    *@Author:HeXun
    *@Description: 根据学生id解锁
    *@Date: 2017/7/25 0025
    */
    public void unlock(){
        String id=getPara("id");
        if(id!=null&&"".equals(id)){
            
        }
    }
    /**
    *@Author:HeXun
    *@Description: 根据学生id重置密码
    *@Date: 2017/7/26 0026
    */
    public void resetPwd(){
        String id=getPara("id");
        if(id!=null&&!"".equals(id)){
            Student stu=Student.dao.findById(id).set("password", Md5Util.MD5("111111"));
            if(stu.update()){
                renderText("重置成功");
            }else{
                renderText("重置失败");
            }
        }
    }

    /**
    *@Author:HeXun
    *@Description: 导入学生表格批量添加学生
    *@Date: 2017/7/26 0026
    */
    public void importStudents(){
        File file=getFile("StudentsList").getFile();
        if(file.getName().endsWith("xls")){
            StudentService studentService=new StudentService();
            if(studentService.importStudents(file)){
                renderText("导入成功");
            }else{
                renderText("导入失败");
            }
        }else{
            renderText("请上传.xls格式文件");
        }
    }

    /**
    *@Author:HeXun
    *@Description: 下载考生导入模版
    *@Date: 2017/7/26 0026
    */
    public void downloadMuban(){
        renderFile("student.xls");
    }

    /**
    *@Author:HeXun
    *@Description: 手动添加考生
    *@Date: 2017/7/27 0027
    */
    public void addStudent(){
        String name=getPara("addStuName").replaceAll(" ","");
        String sno=getPara("addStuNo").replaceAll(" ","");
        String sclass=getPara("addStuScalss").replaceAll(" ","");
        String school=getPara("addStuSchool").replaceAll(" ","");

        new Student().set("name",name).set("sno",sno).set("school",school).set("sclass",sclass).set("password",Md5Util.MD5("111111")).save();
        if(name==null||sno==null||sclass==null||school==null){
            renderText("考生信息不完全，请重新填写");
        }
    }

    public void deleteIn(){
        String id=getPara("id");
        Student.dao.deleteById(id);
        Student stu=Student.dao.findById(id);
        if(stu==null)
        {
            renderText("删除成功");
        }
        else
        {
            renderText("删除失败");
        }
    }

    public void updateStudent(){
        String id=getPara("id");
        String name=getPara("updateStuName");
        String sno=getPara("updateStuNo");
        String sclass=getPara("updateStuScalss");
        String school=getPara("updateStuSchool");
        Student stu=Student.dao.findById(id);
        stu.set("name",name);
        stu.set("sno",sno);
        stu.set("sclass",sclass);
        stu.set("school",school);
        if(stu.update()){
            renderText("修改成功");
        }else{
            renderText("修改失败");
        }
    }
}
