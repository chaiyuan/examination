package com.controller.kaoshi;

import com.jfinal.core.Controller;
import com.model.Course;
import com.model.PaperConfig;
import com.model.Student;
import com.model.StudentExam;
import com.service.kaoshi.Paperservice;
import com.service.mujuan.MujuanService;

/**
 * Created by Hexun on 2017/7/30 0030.
 */
public class PaperController extends Controller {
    Paperservice paperservice = new Paperservice();

    /**
     * @Author:HeXun
     * @Description: 返回学生列表的json数据格式
     * @Date: 2017/7/30 0030
     */
    public void findStudents() {
        String stuCondition = getPara("stuCondition");
        if (stuCondition != null && !"".equals(stuCondition)) {
            renderJson(Student.dao.find("select * from student where sclass like '%" + stuCondition + "%'"));
        } else {
            renderJson(Student.dao.find("select * from student"));
        }
    }

    /**
     * @Author:HeXun
     * @Description: 返回科目列表的json数据格式
     * @Date: 2017/7/30 0030
     */
    public void findKemus() {
        String kemuCondition = getPara("kemuCondition");
        if (kemuCondition != null && !"".equals(kemuCondition)) {
            renderJson(Course.dao.find("select courses.* from courses,paperconfig where courses.id = paperconfig.course_id and paperconfig.id not in (select paperconfig_id from studentpaper ) and name like '%" + kemuCondition + "%'"));
        } else {
            renderJson(Course.dao.find("select courses.* from courses,paperconfig where courses.id = paperconfig.course_id and paperconfig.id not in (select paperconfig_id from studentpaper )"));
        }
    }

    /**
     * @Author:HeXun
     * @Description: 分配考试试卷
     * @Date: 2017/7/30 0030
     */
    public void setPaper() {
        String s = getPara("stuCheckBox");
        Integer[] students_id = getParaValuesToInt("stuCheckBox[]");
        Integer[] kemus_id = getParaValuesToInt("kemuCheckBox[]");
        //根据科目id获取母卷
        for (int kemu_id : kemus_id) {
            PaperConfig paperConfig = paperservice.getPaperConfig(kemu_id);
            int changci = paperConfig.get("exam_no");
            for (int stu_id : students_id) {//将学生和科目添加到考试中
                //判断是否重复
                int isRepeat = paperservice.isRepeat(kemu_id, stu_id);
                if (isRepeat == 0) {
                    new StudentExam().set("sno", stu_id).set("exam_id", changci).set("course_id", kemu_id).set("issubmit", 0).save();
                }
            }
            if (paperservice.allocatePapers(paperConfig) == 1) {
                renderText("分配成功");
            } else {
                renderText("分配失败");
            }
        }
    }
}
