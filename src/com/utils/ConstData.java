package com.utils;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Hexun on 2017/6/6 0006.
 */
public class ConstData {

    //题目类型
    public static final int CHOICE = 1;				// 选择题
    public static final int SIMPLE_FILL = 2;		//简单填空
    public static final int COMPLEX_FILL = 3;		// 复杂填空
    public static final int JUDGE = 4;				// 判断题
    public static final int SIMPlE_PRESS = 5;		// 简答题
    public static final int PROGRAM = 6; 			// 资料分析
    public static final int CHOICE_FILL =7;     	// 多项选择
    public static final int GONGWENXIEZUO = 8; 		// 公文写作
    public static final int CHOICEREAD = 9;          //阅读选择

    //分页每页显示条目
    public static final int SIZE=5;

    /**
     * 映射题目类型与表名
     */
    public Map ques_Map = new TreeMap();
    public Map name_Map = new TreeMap();
    public ConstData(){
        //构造方法，用于初始化ques_Map
        ques_Map.put(new Integer(CHOICE), "Choice");
        ques_Map.put(new Integer(SIMPLE_FILL), "SimpleFill");
        ques_Map.put(new Integer(COMPLEX_FILL), "ComplexFill");
        ques_Map.put(new Integer(JUDGE), "Judge");
        ques_Map.put(new Integer(SIMPlE_PRESS), "SimplePress");
        ques_Map.put(new Integer(PROGRAM), "program");
        ques_Map.put(new Integer(CHOICEREAD), "ChoiceRead");
        ques_Map.put(new Integer(CHOICE_FILL), "ChoiceFill");
        ques_Map.put(new Integer(GONGWENXIEZUO), "Gongwenxiezuo");
        name_Map.put(new Integer(CHOICE), "选择题");
        name_Map.put(new Integer(SIMPLE_FILL), "简单填空");
        name_Map.put(new Integer(COMPLEX_FILL), "复杂填空");
        name_Map.put(new Integer(JUDGE), "判断题");
        name_Map.put(new Integer(SIMPlE_PRESS), "简答题");
        name_Map.put(new Integer(PROGRAM), "资料分析");
        name_Map.put(new Integer(CHOICE_FILL), "多选题");
        name_Map.put(new Integer(GONGWENXIEZUO), "公文写作");
        name_Map.put(new Integer(CHOICEREAD), "阅读选择");
    }
    //根据题型代号获取题型
    public static String showType(int type){
        String s="";
        switch (type){
            case 1:s="选择题";break;
            case 2:s="简单填空";break;
            case 3:s="复杂填空";break;
            case 4:s="判断题";break;
            case 5:s="简答题";break;
            case 6:s="资料分析";break;
            case 7:s="多选题";break;
            case 8:s="公文写作";break;
            case 9:s="阅读选择";break;
        }
        return s;
    }
    //根据题型代号获取题型
    public static String getTimuName(int type){
        String s="";
        switch (type){
            case 1:s="choice";break;
            case 2:s="simplefill";break;
            case 3:s="complexfill";break;
            case 4:s="judge";break;
            case 5:s="simplepress";break;
            case 6:s="program";break;
            case 7:s="choicefill";break;
            case 8:s="gongwenxiezuo";break;
            case 9:s="choiceread";break;
        }
        return s;
    }

    //根据题型获取题型代号
    public static int getTypeInt(String type){
        type=type.trim();
        if("Choice".equalsIgnoreCase(type))
            return 1;
        else if("simplefill".equalsIgnoreCase(type))
            return 2;
        else if("complexfill".equalsIgnoreCase(type))
            return 3;
        else if("judge".equalsIgnoreCase(type))
            return 4;
        else if("simplepress".equalsIgnoreCase(type))
            return 5;
        else if("Program".equalsIgnoreCase(type))
            return 6;
        else if("ChoiceFill".equalsIgnoreCase(type))
            return 7;
        else if("gongwenxiezuo".equalsIgnoreCase(type))
            return 8;
        else if("choiceread".equalsIgnoreCase(type))
            return 9;
        else
            return 0;
    }
}
