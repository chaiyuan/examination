package com.utils;

import com.model.OriginPaper;

import java.util.*;

/**
 * Created by Hexun on 2017/7/30 0030.
 */
public class Tool {
    public static Map<Integer,List<OriginPaper>> questionFilter(List<OriginPaper> list) {
        Map<Integer,List<OriginPaper>> map=new TreeMap<>();
        for(OriginPaper originPaper:list){
            if(map.containsKey(originPaper.get("ques_type"))){
                map.get(originPaper.get("ques_type")).add(originPaper);
            }else{
                List li=new ArrayList<OriginPaper>();
                li.add(originPaper);
                map.put((Integer) originPaper.get("ques_type"),li);
            }
        }
        return map;
    }
    public static List<Integer> getRandom(int num){
        List<Integer> numList = new ArrayList<Integer>();
        for(int i=0;i<num;i++){
            Random r = new Random();
            boolean b;
            int n;
            do{
                b = false;
                n = r.nextInt(num);
                for(int j=0;j<numList.size();j++){
                    if(((Integer) numList.get(j)).intValue()==n){
                        b = true;
                        break;
                    }
                }
            }while(b);
            numList.add(new Integer(n));
        }
        return numList;
    }
    public static List<Integer> getRandom(int start,int end){
        List<Integer> numList = new ArrayList<Integer>();
        for(int i=start;i<end;i++){
            Random random = new Random();
            boolean bool;
            int n;
            do{
                bool = false;
                n = random.nextInt(end);
                if(n >= start){
                    for(int j=0;j<numList.size();j++){
                        if(((Integer) numList.get(j)).intValue()==n){
                            bool = true;
                            break;
                        }
                    }
                }else{
                    bool = true;
                }
            }while(bool);
            numList.add(new Integer(n));
        }
        return numList;
    }
}
