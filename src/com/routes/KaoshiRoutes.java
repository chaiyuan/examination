package com.routes;

import com.controller.kaoshi.ExamController;
import com.controller.kaoshi.PaperController;
import com.controller.kaoshi.StudentController;
import com.jfinal.config.Routes;

/**
 * Created by Hexun on 2017/7/25 0025.
 */
public class KaoshiRoutes extends Routes{
    @Override
    public void config() {
        setBaseViewPath("/view");
        add("student", StudentController.class);
        add("exam", ExamController.class);
        add("paper", PaperController.class);
    }
}
