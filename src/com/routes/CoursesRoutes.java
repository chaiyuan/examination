package com.routes;

import com.controller.CourseController;
import com.jfinal.config.Routes;

/**
 * Created by Hexun on 2017/5/28 0028.
 */
public class CoursesRoutes extends Routes{
    public void config(){
        setBaseViewPath("/view");
        add("/courses", CourseController.class);
    }
}
