package com.routes;

import com.controller.mujuan.MujuanController;
import com.jfinal.config.Routes;

/**
 * Created by Hexun on 2017/6/22 0022.
 */
public class MujuanRoutes extends Routes{
    public void config(){
        setBaseViewPath("/view");
        add("/mujuan", MujuanController.class);
    }
}
