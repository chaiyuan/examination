package com.routes;

import com.controller.tiku.TikuImportController;
import com.controller.tiku.TikuManageController;
import com.jfinal.config.Routes;

/**
 * Created by Hexun on 2017/5/30 0030.
 */
public class TikuRoutes extends Routes{
    public void config(){
        setBaseViewPath("/view");
        add("/tiku", TikuImportController.class);
        add("/tikuManage", TikuManageController.class);
    }
}
