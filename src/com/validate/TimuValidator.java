package com.validate;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

/**
 * Created by Hexun on 2017/6/9 0009.
 */
public class TimuValidator extends Validator{
    public void validate(Controller c){
        setShortCircuit(true);
        validateRequiredString("body","Result","题干不能为空");
        validateRequiredString("point","Result","分值不能为空");
        validateRequiredString("answer","Result","答案不能为空");
    }

    public void handleError(Controller c){
        c.keepPara();
        c.render("/view/tiku/TimuModify.jsp");
    }
}
