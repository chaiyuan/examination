package com.validate;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

/**
 * Created by Hexun on 2017/6/1 0001.
 */
public class validateKemu extends Validator{
    public void validate(Controller c){
        validateRequiredString("kemu","kemuNull","科目不能为空");
        validateKemuNames("kemu","kemuNamesWrong","科目格式错误！");
    }
    public void handleError(Controller c){
        c.keepPara();
        c.render("CourseManage.jsp");
    }

    protected void validateKemuNames(String field,String erroykey,String erroMessage){
        String kemu= controller.getPara(field);
        String[] array=kemu.split("-");
        if(array.length==2){
            String nianji = array[0];
            String course = array[1];
            if ( "".equals(nianji) || nianji == null) {
                addError("Result","输入科目格式不正确,年级不能为空");
            } else if ("".equals(course) || course == null) {
                addError("Result","输入科目格式不正确,科目不能为空");
            }
        }else{
            addError("Result","输入科目格式不正确");
        }
    }
}
