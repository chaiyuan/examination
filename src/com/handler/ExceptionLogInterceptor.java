package com.handler;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.JFinal;
import org.apache.log4j.Logger;

/**
 * Created by Hexun on 2017/6/13 0013.
 */
public class ExceptionLogInterceptor implements Interceptor {
    private static final Logger log = Logger.getLogger(ExceptionLogInterceptor.class);
    public void intercept(Invocation invocation){
        try {
            invocation.invoke();
        } catch (Exception e) {
            //记录到日志文件中
            logWrite(invocation, e);
        }
    }
    /**
    *@Author: HeXun
    *@Description: 将异常记录到日志中
    *@Date: 2017/6/13 0013
    */
    private void logWrite(Invocation inv,Exception e){
        //打印日志
        StringBuilder sb =new StringBuilder("\n---Exception Log Begin---\n");
        sb.append("Controller:").append(inv.getController().getClass().getName()).append("\n");
        sb.append("Method:").append(inv.getMethodName()).append("\n");
        sb.append("Exception Type:").append(e.getClass().getName()).append("\n");
        sb.append("Exception Details:");
        log.error(sb.toString(),e);
    }
}
