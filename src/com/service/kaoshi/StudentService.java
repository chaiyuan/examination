package com.service.kaoshi;

import com.model.Student;
import com.utils.Md5Util;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import java.io.File;
import java.io.IOException;

/**
 * Created by Hexun on 2017/7/26 0026.
 */
public class StudentService {
    public boolean importStudents(File file){
        Workbook workbook=null;
        try {
            workbook=Workbook.getWorkbook(file);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
        Sheet sheet=workbook.getSheet(0);
        int row=0;
        boolean result=false;
        while(row<sheet.getRows()){
            Cell[] cells=sheet.getRow(row++);
            Student s=new Student();
            if(cells[0].getContents().toString()!=""){
                s.set("sno",cells[2].getContents().toString());
                s.set("name",cells[3].getContents());
                s.set("sclass",cells[1].getContents().toString());
                s.set("password",Md5Util.MD5("111111"));
                s.set("school",cells[0].getContents());
                if(s.save()){
                    result=true;
                }else{
                    result=false;
                }
            }
        }
        file.delete();
        return result;
    }

}
