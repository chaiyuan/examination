package com.service.kaoshi;

import com.model.*;
import com.utils.Tool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hexun on 2017/7/30 0030.
 */
public class Paperservice {
    /**
     * @Author:HeXun
     * @Description: 通过科目id获取母卷
     * @Date: 2017/7/30 0030
     */
    public PaperConfig getPaperConfig(int kemu_id) {
        return PaperConfig.dao.findFirst("select * from paperconfig where course_id=?", kemu_id);
    }

    /**
     * @Author:HeXun
     * @Description: 通过
     * @Date: 2017/7/30 0030
     */
    public int isRepeat(int kemu_id, int stu_id) {
        List list = StudentExam.dao.find("select * from studentexam where course_id=? and sno = ?", kemu_id, stu_id);
        if (list.size() > 0) {
            return 1;
        }
        return 0;
    }

    /**
     * @Author:HeXun
     * @Description: 为考生分配试卷
     * @Date: 2017/7/30 0030
     */
    public int allocatePapers(PaperConfig paperConfig) {
        List<StudentPaper> listSP = new ArrayList();
        List<StudentExam> studentExamList = StudentExam.dao.find("select * from studentexam where course_id=? and exam_id=?", paperConfig.get("course_id"), paperConfig.get("exam_no"));
        List<OriginPaper> originPaperList = OriginPaper.dao.find("select * from originpaper where paperconfig_id=?", paperConfig.get("id"));
        if (originPaperList.size() == 0) {
            return 0;
        }
        Map<Integer, List<OriginPaper>> map = Tool.questionFilter(originPaperList);
        for (StudentExam studentExam : studentExamList) {
            for (Map.Entry<Integer, List<OriginPaper>> entry : map.entrySet()) {
                int type = entry.getKey();
                if (type == 9) {
                    int listSize = 0;
                    List<OriginPaper> list = entry.getValue();
                    List<ChoiceRead> listTemp = new ArrayList<>();
                    Map<Integer, Integer> QCount = new HashMap<>();
                    for (OriginPaper originPaper : list) {
                        ChoiceRead choiceRead = ChoiceRead.dao.findById(originPaper.get("ques_id"));
                        listTemp.add(choiceRead);
                        QCount.put((Integer) originPaper.get("ques_sq"), (Integer) originPaper.get("ques_sq"));
                    }
                    for (Map.Entry<Integer, Integer> qc : QCount.entrySet()) {
                        int sequence = qc.getValue();
                        List<ChoiceRead> listChoiceRead = new ArrayList<ChoiceRead>();
                        for (ChoiceRead cr : listTemp) {
                            if ((Integer) cr.get("ques_sq") == sequence) {
                                listChoiceRead.add(cr);
                            }
                        }
                        List numList = null;
                        if (listSize == 0) {
                            listSize += listChoiceRead.size();
                            numList = Tool.getRandom(listSize);
                        } else {
                            int start = listSize;
                            listSize += listChoiceRead.size();
                            int end = listSize;
                            numList = Tool.getRandom(start, end);
                        }
                        for (int l = 0; l < listChoiceRead.size(); l++) {
                            StudentPaper studentPaper = new StudentPaper();
                            studentPaper.set("sno", studentExam.get("sno")).set("ques_type", type).set("status", 0).set("ques_id", Integer.parseInt(""
                                    + listChoiceRead.get(l).get("id"))).set("paperconfig_id", paperConfig.get("id"));
                            listSP.add(studentPaper);

                        }
                    }
                } else {
                    // 修改部分end
                    List list = (List) map.get(type);
                    int ls = list.size();
                    List numList = Tool.getRandom(ls);
                    for (int j = 0; j < ls; j++) {
                        OriginPaper originPaper = (OriginPaper) list.get(j);
                        StudentPaper studentPaper = null;
                        if (studentPaper == null) {
                            studentPaper = new StudentPaper();
                        }
                        studentPaper.set("sno", studentExam.get("sno")).set("ques_type", type).set("status", 0).set("ques_id", originPaper.get("ques_id")).set("ques_sq", ((Integer) numList.get(j)).intValue()).set("paperconfig_id", paperConfig.get("id"));
                        listSP.add(studentPaper);
                    }
                }
            }
        }
        int res = 1;
        for (StudentPaper s : listSP) {
            if (!s.save()) {
                res = 0;
            }
        }
        return res;
    }
}
