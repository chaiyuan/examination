package com.bean;

import com.jfinal.plugin.activerecord.Record;
import com.utils.ConstData;

import java.util.List;

/**
 * Created by Hexun on 2017/6/22 0022.
 */

/**
 * 自动组卷bean
 * courseId 	科目号
 * changci 		所要组母卷的的场次数目
 * chapter 		题目章节
 * rate 		题目难度系数 从1到5变化
 * type 		题目类型
 * timuType     题目类型 中文表示
 * dBNumber 	数据库中本题所含题目数量
 * giveNumber 	设定的母卷的题目数量
 * <p>
 * String[]questionID
 */
public class AutoOriginPaperBean {
    private List questionID;
    private int courseId;
    private int changci;
    private String chapter;
    private int rate;
    private int type;
    private String timuType;
    private int dBNumber;
    private int giveNumber;
    private String point;
    public AutoOriginPaperBean() {
    }

    public List<Record> getQuestionID() {
        return questionID;
    }

    public AutoOriginPaperBean(List questionID, int courseId, String chapter,
                               int rate, int type, String point) {
        this.questionID = questionID;
        this.courseId = courseId;
        this.chapter = chapter;
        this.rate = rate;
        this.type = type;
        this.point = point;
        this.timuType=new ConstData().showType(type);
    }

    public String getTimuType() {
        return timuType;
    }

    public void setTimuType(String timuType) {
        this.timuType = timuType;
    }
    public void setQuestionID(List questionID) {
        this.questionID = questionID;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getChangci() {
        return changci;
    }

    public void setChangci(int changci) {
        this.changci = changci;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getdBNumber() {
        return dBNumber;
    }

    public void setdBNumber(int dBNumber) {
        this.dBNumber = dBNumber;
    }

    public int getGiveNumber() {
        return giveNumber;
    }

    public void setGiveNumber(int giveNumber) {
        this.giveNumber = giveNumber;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

}
