package com.bean;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.generator.BaseModelGenerator;
import com.model.Course;
import com.model.OpenTime;

/**
 * Created by Hexun on 2017/6/18 0018.
 */
public class openKemu {


    private Model<Course> courseModel;
    private Model<OpenTime> openTimeModel;

    public Model<OpenTime> getOpenTimeModel() {
        return openTimeModel;
    }

    public void setOpenTimeModel(Model<OpenTime> openTimeModel) {
        this.openTimeModel = openTimeModel;
    }

    public Model<Course> getCourseModel() {

        return courseModel;
    }

    public void setCourseModel(Model<Course> courseModel) {
        this.courseModel = courseModel;
    }
}
