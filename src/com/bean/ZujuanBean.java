package com.bean;

/**
 * Created by Hexun on 2017/7/2 0002.
 */

import com.model.Course;

import java.util.List;


public class ZujuanBean {
    private Course yuwen;
    private Course shuxue;
    private Course zhengzhi;
    private List<Integer> yuwenList = null;
    private List<Integer> shuxueList = null;
    private List<Integer> zhengzhiList = null;

    public Course getYuwen() {
        return yuwen;
    }

    public void setYuwen(Course yuwen) {
        this.yuwen = yuwen;
    }

    public Course getShuxue() {
        return shuxue;
    }

    public void setShuxue(Course shuxue) {
        this.shuxue = shuxue;
    }

    public Course getZhengzhi() {
        return zhengzhi;
    }

    public void setZhengzhi(Course zhengzhi) {
        this.zhengzhi = zhengzhi;
    }

    public List<Integer> getYuwenList() {
        return yuwenList;
    }

    public void setYuwenList(List<Integer> yuwenList) {
        this.yuwenList = yuwenList;
    }

    public List<Integer> getShuxueList() {
        return shuxueList;
    }

    public void setShuxueList(List<Integer> shuxueList) {
        this.shuxueList = shuxueList;
    }

    public List<Integer> getZhengzhiList() {
        return zhengzhiList;
    }

    public void setZhengzhiList(List<Integer> zhengzhiList) {
        this.zhengzhiList = zhengzhiList;
    }

}

