import com.controller.IndexController;
import com.handler.ExceptionLogInterceptor;
import com.handler.XSSHandler;
import com.jfinal.config.*;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import com.model.*;
import com.routes.CoursesRoutes;
import com.routes.KaoshiRoutes;
import com.routes.MujuanRoutes;
import com.routes.TikuRoutes;

/**
 * Created by chaiyuan on 2017/5/23.
 */
public class AppConfig extends JFinalConfig {
    @Override
    public void configConstant(Constants me) {
        me.setDevMode(true);
        me.setViewType(ViewType.JSP);
        me.setBaseDownloadPath("WEB-INF/download");
        me.setBaseUploadPath("WEB-INF/upload");
    }
    @Override
    public void configRoute(Routes me) {
        me.add("/index",IndexController.class);
        me.add(new CoursesRoutes());
        me.add(new TikuRoutes());
        me.add(new MujuanRoutes());
        me.add(new KaoshiRoutes());
    }

    @Override
    public void configEngine(Engine me) {

    }

    @Override
    public void configPlugin(Plugins me) {
        C3p0Plugin cp = new C3p0Plugin("jdbc:mysql://localhost:3306/exam", "root", "123456");
        me.add(cp);
        ActiveRecordPlugin arp = new ActiveRecordPlugin(cp);
        me.add(arp);
        arp.setShowSql(true);//显示sql语句
        //默认主键名为id
        arp.addMapping("courses",Course.class);
        arp.addMapping("choice",Choice.class);
        arp.addMapping("judge", Judge.class);
        arp.addMapping("simplefill", SimpleFill.class);
        arp.addMapping("choiceread",ChoiceRead.class);
        arp.addMapping("simplepress",SimplePress.class);
        arp.addMapping("program",Program.class);
        arp.addMapping("choicefill",ChoiceFill.class);
        arp.addMapping("gongwenxiezuo",Gongwenxiezuo.class);
        arp.addMapping("complexfill",ComplexFill.class);
        arp.addMapping("opentime",OpenTime.class);
        arp.addMapping("paperconfig",PaperConfig.class);
        arp.addMapping("originpaper",OriginPaper.class);
        arp.addMapping("student",Student.class);
        arp.addMapping("studentexam",StudentExam.class);
        arp.addMapping("studentpaper",StudentPaper.class);
    }

    @Override
    public void configInterceptor(Interceptors me) {
        //添加日志拦截器,记录异常
        me.addGlobalActionInterceptor(new ExceptionLogInterceptor());
    }

    @Override
    public void configHandler(Handlers me) {
        me.add(new XSSHandler("//"));
    }

}
